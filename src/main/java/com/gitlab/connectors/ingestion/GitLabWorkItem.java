package com.gitlab.connectors.ingestion;

import java.util.Map;
import java.util.Optional;

import com.gitlab.connectors.ingestion.exception.WorkItemPayloadPropertyNotFoundException;
import com.gitlab.connectors.ingestion.exception.WorkItemResourceIdPropertyNotFoundException;
import com.snowflake.connectors.taskreactor.worker.queue.WorkItem;
import com.snowflake.snowpark_java.types.Variant;

public class GitLabWorkItem {
  private static final String TYPE_PROPERTY = "type";
  private static final String GROUP_NAME_PROPERTY = "group_name";
  private static final String PROJECT_NAME_PROPERTY = "project_name";
  private static final String RESOURCE_INGESTION_DEFINITION_ID_PROPERTY =
      "resourceIngestionDefinitionId";
  private static final String INGESTION_CONFIGURATION_ID_PROPERTY = "ingestionConfigurationId";
  private static final String RESOURCE_ID_PROPERTY = "resourceId";

  private final String type;
  private final String groupName;
  private final String projectName;
  private final String processId;
  private final String resourceIngestionDefinitionId;
  private final String ingestionConfigurationId;

  public static GitLabWorkItem from(WorkItem workItem) {
    String type = extractResourceIdProperty(workItem, TYPE_PROPERTY);

    String groupName = null;
    if (type.equals("group")) {
      groupName = extractResourceIdProperty(workItem, GROUP_NAME_PROPERTY);
    }

    String projectName = null;
    if (type.equals("project")) {
      projectName = extractResourceIdProperty(workItem, PROJECT_NAME_PROPERTY);
    }

    String resourceIngestionDefinitionId =
        extractPropertyFromWorkItemPayload(workItem, RESOURCE_INGESTION_DEFINITION_ID_PROPERTY);
    String ingestionConfigurationId =
        extractPropertyFromWorkItemPayload(workItem, INGESTION_CONFIGURATION_ID_PROPERTY);
    return new GitLabWorkItem(
        type,
        groupName,
        projectName,
        workItem.resourceId,
        resourceIngestionDefinitionId,
        ingestionConfigurationId);
  }

  private static String extractPropertyFromWorkItemPayload(WorkItem workItem, String propertyName) {
    Map<String, Variant> properties = workItem.payload.asMap();
    return Optional.ofNullable(properties.get(propertyName))
        .map(Variant::asString)
        .orElseThrow(() -> new WorkItemPayloadPropertyNotFoundException(propertyName));
  }

  private static String extractResourceIdProperty(WorkItem workItem, String propertyName) {
    Map<String, Variant> properties = workItem.payload.asMap();
    return Optional.ofNullable(properties.get(RESOURCE_ID_PROPERTY))
        .map(Variant::asMap)
        .map(it -> it.get(propertyName))
        .map(Variant::asString)
        .orElseThrow(() -> new WorkItemResourceIdPropertyNotFoundException(propertyName));
  }

  private GitLabWorkItem(
      String type,
      String groupName,
      String projectName,
      String processId,
      String resourceIngestionDefinitionId,
      String ingestionConfigurationId) {
    this.type = type;
    this.groupName = groupName;
    this.projectName = projectName;
    this.processId = processId;
    this.resourceIngestionDefinitionId = resourceIngestionDefinitionId;
    this.ingestionConfigurationId = ingestionConfigurationId;
  }

  public String getType() {
    return type;
  }

  public boolean isInstanceType() {
    return type.equals("instance");
  }

  public boolean isGroupType() {
    return type.equals("group");
  }

  public boolean isProjectType() {
    return type.equals("project");
  }

  public String getGroupName() {
    return groupName;
  }

  public String getProjectName() {
    return projectName;
  }

  public String getProcessId() {
    return processId;
  }

  public String getResourceIngestionDefinitionId() {
    return resourceIngestionDefinitionId;
  }

  public String getIngestionConfigurationId() {
    return ingestionConfigurationId;
  }
}
