package com.gitlab.connectors.ingestion.project;

import static com.gitlab.connectors.ConnectorObjects.*;

import com.gitlab.connectors.http.GitlabHttpHelper;
import com.gitlab.connectors.ingestion.GitLabWorkItem;
import com.gitlab.connectors.ingestion.IngestionHelper;
import com.gitlab.connectors.ingestion.utils.Ingestion;
import com.snowflake.snowpark_java.Functions;
import com.snowflake.snowpark_java.Row;
import com.snowflake.snowpark_java.Session;

public class IngestProjectAuditEventsService {
  public static long execute(Session session, GitLabWorkItem gitLabWorkItem) {
    String destinationTable =
        Ingestion.getDatabaseTableObjectName(session, PROJECT_AUDIT_EVENTS_TABLE).getEscapedName();
    String latestCreatedAt = getLatestCreatedAt(session, gitLabWorkItem.getProjectName());
    var rawResults =
        new GitlabHttpHelper(session)
            .fetchProjectAuditEvents(gitLabWorkItem.getProjectName(), latestCreatedAt);
    return IngestionHelper.saveProjectAuditEventsData(
        session, destinationTable, rawResults, gitLabWorkItem);
  }

  private static String getLatestCreatedAt(Session session, String projectName) {
    String projectAuditEventsView =
        Ingestion.getDatabaseTableObjectName(session, PROJECT_AUDIT_EVENTS_VIEW).getEscapedName();

    Row[] latestRowByCreatedAt =
        session
            .table(projectAuditEventsView)
            .where(Functions.col("project_name").equal_to(Functions.lit(projectName)))
            .select(Functions.col("created_at"))
            .sort(Functions.col("created_at").desc())
            .limit(1)
            .collect();

    if (latestRowByCreatedAt.length != 0) {
      return latestRowByCreatedAt[0].getString(0).replaceAll("\"", "");
    }
    return null;
  }
}
