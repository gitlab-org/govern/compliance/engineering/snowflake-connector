package com.gitlab.connectors.ingestion.group;

import static com.gitlab.connectors.ConnectorObjects.*;

import com.gitlab.connectors.http.GitlabHttpHelper;
import com.gitlab.connectors.ingestion.GitLabWorkItem;
import com.gitlab.connectors.ingestion.IngestionHelper;
import com.gitlab.connectors.ingestion.utils.Ingestion;
import com.snowflake.snowpark_java.Functions;
import com.snowflake.snowpark_java.Row;
import com.snowflake.snowpark_java.Session;

public class IngestGroupAuditEventsService {
  public static long execute(Session session, GitLabWorkItem gitLabWorkItem) {
    String destinationTable =
        Ingestion.getDatabaseTableObjectName(session, GROUP_AUDIT_EVENTS_TABLE).getEscapedName();
    String latestCreatedAt = getLatestCreatedAt(session, gitLabWorkItem.getGroupName());
    var rawResults =
        new GitlabHttpHelper(session)
            .fetchGroupAuditEvents(gitLabWorkItem.getGroupName(), latestCreatedAt);
    return IngestionHelper.saveGroupAuditEventsData(
        session, destinationTable, rawResults, gitLabWorkItem);
  }

  private static String getLatestCreatedAt(Session session, String groupName) {
    String groupAuditEventsView =
        Ingestion.getDatabaseTableObjectName(session, GROUP_AUDIT_EVENTS_VIEW).getEscapedName();

    Row[] latestRowByCreatedAt =
        session
            .table(groupAuditEventsView)
            .where(Functions.col("group_name").equal_to(Functions.lit(groupName)))
            .select(Functions.col("created_at"))
            .sort(Functions.col("created_at").desc())
            .limit(1)
            .collect();

    if (latestRowByCreatedAt.length != 0) {
      return latestRowByCreatedAt[0].getString(0).replaceAll("\"", "");
    }
    return null;
  }
}
