package com.gitlab.connectors.ingestion.exception;

public class WorkItemResourceIdPropertyNotFoundException extends RuntimeException {

  private static final String ERROR_MESSAGE = "Property [%s] not found in the WorkItem resourceId";

  public WorkItemResourceIdPropertyNotFoundException(String property) {
    super(String.format(ERROR_MESSAGE, property));
  }
}
