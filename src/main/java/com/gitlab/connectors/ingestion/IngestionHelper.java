package com.gitlab.connectors.ingestion;

import static com.snowflake.snowpark_java.Functions.col;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.snowflake.snowpark_java.Row;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.DataTypes;
import com.snowflake.snowpark_java.types.StructField;
import com.snowflake.snowpark_java.types.StructType;
import com.snowflake.snowpark_java.types.Variant;

/** A utility class for resource ingestion handling. */
public final class IngestionHelper {

  private IngestionHelper() {}

  /**
   * Deduplicate records based on ID field
   *
   * @param data Raw data to deduplicate
   * @return List of unique records
   */
  private static List<Variant> deduplicateById(List<Variant> data) {
    if (data == null || data.isEmpty()) {
      return new ArrayList<>();
    }

    var uniqueData = new ArrayList<Variant>();
    var seenIds = new java.util.HashSet<String>();

    for (Variant record : data) {
      if (record != null) {
        Map<String, Variant> recordMap = record.asMap();
        if (recordMap != null && recordMap.containsKey("id")) {
          String id = recordMap.get("id").asString();
          if (!seenIds.contains(id)) {
            seenIds.add(id);
            uniqueData.add(record);
          }
        }
      }
    }

    return uniqueData;
  }

  /**
   * Save the raw data to a database table
   *
   * @param session Snowpark session object
   * @param destTableName Target table name
   * @param data Raw data to save
   */
  public static long saveInstanceData(Session session, String destTableName, List<Variant> data) {
    var uniqueData = deduplicateById(data);
    if (uniqueData.isEmpty()) {
      return 0;
    }

    var tableSchema = StructType.create(new StructField("RAW_DATA", DataTypes.VariantType));
    var dataRows = uniqueData.stream().map(Row::create).toArray(Row[]::new);
    var source = session.createDataFrame(dataRows, tableSchema);
    var table = session.table(destTableName);
    var assignments = Map.of(col("RAW_DATA"), source.col("RAW_DATA"));
    table
        .merge(
            source,
            table.col("raw_data").subField("id").equal_to(source.col("raw_data").subField("id")))
        .whenMatched()
        .update(assignments)
        .whenNotMatched()
        .insert(assignments)
        .collect();
    return dataRows.length;
  }

  /**
   * Update existing records in the database table without inserting new ones
   *
   * @param session Snowpark session object
   * @param destTableName Target table name
   * @param data Raw data to update
   */
  public static long updateExistingInstanceData(
      Session session, String destTableName, List<Variant> data) {
    var uniqueData = deduplicateById(data);
    if (uniqueData.isEmpty()) {
      return 0;
    }

    var tableSchema = StructType.create(new StructField("RAW_DATA", DataTypes.VariantType));
    var dataRows = uniqueData.stream().map(Row::create).toArray(Row[]::new);
    var source = session.createDataFrame(dataRows, tableSchema);
    var table = session.table(destTableName);
    var assignments = Map.of(col("RAW_DATA"), source.col("RAW_DATA"));
    table
        .merge(
            source,
            table.col("raw_data").subField("id").equal_to(source.col("raw_data").subField("id")))
        .whenMatched()
        .update(assignments)
        // Intentionally omitting whenNotMatched().insert() to only update existing records
        .collect();
    return dataRows.length;
  }

  /**
   * Save group audit events data to a database table
   *
   * @param session Snowpark session object
   * @param destTableName Target table name
   * @param data Raw data to save
   * @param gitLabWorkItem GitLab work item containing group information
   */
  public static long saveGroupAuditEventsData(
      Session session, String destTableName, List<Variant> data, GitLabWorkItem gitLabWorkItem) {
    var tableSchema =
        StructType.create(
            new StructField("GROUP_NAME", DataTypes.StringType),
            new StructField("RAW_DATA", DataTypes.VariantType));

    var dataRows =
        data.stream().map(it -> Row.create(gitLabWorkItem.getGroupName(), it)).toArray(Row[]::new);

    var source = session.createDataFrame(dataRows, tableSchema);
    var table = session.table(destTableName);
    var assignments =
        Map.of(
            col("GROUP_NAME"), source.col("GROUP_NAME"),
            col("RAW_DATA"), source.col("RAW_DATA"));
    table
        .merge(
            source,
            table.col("raw_data").subField("id").equal_to(source.col("raw_data").subField("id")))
        .whenMatched()
        .update(assignments)
        .whenNotMatched()
        .insert(assignments)
        .collect();
    return dataRows.length;
  }

  /**
   * Save project audit events data to a database table
   *
   * @param session Snowpark session object
   * @param destTableName Target table name
   * @param data Raw data to save
   * @param gitLabWorkItem GitLab work item containing project information
   */
  public static long saveProjectAuditEventsData(
      Session session, String destTableName, List<Variant> data, GitLabWorkItem gitLabWorkItem) {
    var tableSchema =
        StructType.create(
            new StructField("PROJECT_NAME", DataTypes.StringType),
            new StructField("RAW_DATA", DataTypes.VariantType));

    var dataRows =
        data.stream()
            .map(it -> Row.create(gitLabWorkItem.getProjectName(), it))
            .toArray(Row[]::new);

    var source = session.createDataFrame(dataRows, tableSchema);
    var table = session.table(destTableName);
    var assignments =
        Map.of(
            col("PROJECT_NAME"), source.col("PROJECT_NAME"),
            col("RAW_DATA"), source.col("RAW_DATA"));
    table
        .merge(
            source,
            table.col("raw_data").subField("id").equal_to(source.col("raw_data").subField("id")))
        .whenMatched()
        .update(assignments)
        .whenNotMatched()
        .insert(assignments)
        .collect();
    return dataRows.length;
  }
}
