package com.gitlab.connectors.ingestion.instance;

import static com.gitlab.connectors.ConnectorObjects.INSTANCE_AUDIT_EVENTS_TABLE;
import static com.gitlab.connectors.ConnectorObjects.INSTANCE_AUDIT_EVENTS_VIEW;

import com.gitlab.connectors.http.GitlabHttpHelper;
import com.gitlab.connectors.ingestion.IngestionHelper;
import com.gitlab.connectors.ingestion.utils.Ingestion;
import com.snowflake.snowpark_java.Functions;
import com.snowflake.snowpark_java.Row;
import com.snowflake.snowpark_java.Session;

public class IngestAuditEventsService {
  public static long execute(Session session) {
    String destinationTable =
        Ingestion.getDatabaseTableObjectName(session, INSTANCE_AUDIT_EVENTS_TABLE).getEscapedName();
    String latestCreatedAt = getLatestCreatedAt(session);
    var rawResults = new GitlabHttpHelper(session).fetchInstanceAuditEvents(latestCreatedAt);
    return IngestionHelper.saveInstanceData(session, destinationTable, rawResults);
  }

  private static String getLatestCreatedAt(Session session) {
    String instanceAuditEventsView =
        Ingestion.getDatabaseTableObjectName(session, INSTANCE_AUDIT_EVENTS_VIEW).getEscapedName();
    Row[] latestRowByCreatedAt =
        session
            .table(instanceAuditEventsView)
            .select(Functions.col("created_at"))
            .sort(Functions.col("created_at").desc())
            .limit(1)
            .collect();

    if (latestRowByCreatedAt.length != 0) {
      return latestRowByCreatedAt[0].getString(0).replaceAll("\"", "");
    }
    return null;
  }
}
