package com.gitlab.connectors.ingestion.instance;

import static com.gitlab.connectors.ConnectorObjects.*;
import static com.gitlab.connectors.ingestion.utils.Ingestion.isGitLabDotCom;

import com.gitlab.connectors.http.GitlabHttpHelper;
import com.gitlab.connectors.ingestion.IngestionHelper;
import com.gitlab.connectors.ingestion.utils.Ingestion;
import com.snowflake.snowpark_java.Functions;
import com.snowflake.snowpark_java.Row;
import com.snowflake.snowpark_java.Session;

public class IngestUsersService {
  public static long execute(Session session) {
    // Don't ingest users data for GitLab.com
    if (isGitLabDotCom(session)) {
      return 0;
    }

    String destinationTable =
        Ingestion.getDatabaseTableObjectName(session, USERS_TABLE).getEscapedName();
    String latestCreatedAt = getLatestCreatedAt(session);
    var rawResults = new GitlabHttpHelper(session).fetchUsers(latestCreatedAt);
    return IngestionHelper.saveInstanceData(session, destinationTable, rawResults);
  }

  private static String getLatestCreatedAt(Session session) {
    String usersView = Ingestion.getDatabaseTableObjectName(session, USERS_VIEW).getEscapedName();
    Row[] latestRowByCreatedAt =
        session
            .table(usersView)
            .select(Functions.col("created_at"))
            .sort(Functions.col("created_at").desc())
            .limit(1)
            .collect();

    if (latestRowByCreatedAt.length != 0) {
      return latestRowByCreatedAt[0].getString(0).replaceAll("\"", "");
    }
    return null;
  }
}
