package com.gitlab.connectors.ingestion.instance;

import static com.gitlab.connectors.ConnectorObjects.*;

import com.gitlab.connectors.http.GitlabHttpHelper;
import com.gitlab.connectors.ingestion.IngestionHelper;
import com.gitlab.connectors.ingestion.utils.Ingestion;
import com.snowflake.snowpark_java.Functions;
import com.snowflake.snowpark_java.Row;
import com.snowflake.snowpark_java.Session;

public class IngestProjectsService {
  public static long execute(Session session) {
    String destinationTable =
        Ingestion.getDatabaseTableObjectName(session, PROJECTS_TABLE).getEscapedName();
    String projectsView =
        Ingestion.getDatabaseTableObjectName(session, PROJECTS_VIEW).getEscapedName();

    // Get sync states
    String largestId = getLargestProjectId(session, projectsView);
    String latestUpdatedAt = getLatestUpdatedAt(session, projectsView);

    long totalIngested = 0;

    // Case 1: Initial sync (no existing data)
    if (largestId == null && latestUpdatedAt == null) {
      var initialResults = new GitlabHttpHelper(session).fetchProjects(null, null);
      return IngestionHelper.saveInstanceData(session, destinationTable, initialResults);
    }

    // Case 2: Get new projects (after our largest known ID)
    if (largestId != null) {
      var newResults = new GitlabHttpHelper(session).fetchProjects(largestId, null);
      totalIngested += IngestionHelper.saveInstanceData(session, destinationTable, newResults);
    }

    // Case 3: Get updates to existing projects (updates only, no new insertions)
    if (latestUpdatedAt != null) {
      var updatedResults = new GitlabHttpHelper(session).fetchProjects(null, latestUpdatedAt);
      totalIngested +=
          IngestionHelper.updateExistingInstanceData(session, destinationTable, updatedResults);
    }

    return totalIngested;
  }

  private static String getLargestProjectId(Session session, String projectsView) {
    Row[] latestRowById =
        session
            .table(projectsView)
            .select(Functions.col("id"))
            .sort(Functions.col("id").desc())
            .limit(1)
            .collect();

    if (latestRowById.length != 0) {
      return latestRowById[0].getString(0).replaceAll("\"", "");
    }
    return null;
  }

  private static String getLatestUpdatedAt(Session session, String projectsView) {
    Row[] latestRowByUpdatedAt =
        session
            .table(projectsView)
            .select(Functions.col("updated_at"))
            .sort(Functions.col("updated_at").desc())
            .limit(1)
            .collect();

    if (latestRowByUpdatedAt.length != 0) {
      return latestRowByUpdatedAt[0].getString(0).replaceAll("\"", "");
    }
    return null;
  }
}
