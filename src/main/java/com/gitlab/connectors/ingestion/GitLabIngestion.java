package com.gitlab.connectors.ingestion;

import static com.snowflake.connectors.application.observability.IngestionRun.IngestionStatus.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.connectors.ingestion.group.IngestGroupAuditEventsService;
import com.gitlab.connectors.ingestion.instance.IngestAuditEventsService;
import com.gitlab.connectors.ingestion.instance.IngestProjectsService;
import com.gitlab.connectors.ingestion.instance.IngestUsersService;
import com.gitlab.connectors.ingestion.project.IngestProjectAuditEventsService;
import com.snowflake.connectors.application.observability.IngestionRun;
import com.snowflake.connectors.application.observability.IngestionRunRepository;
import com.snowflake.connectors.taskreactor.OnIngestionFinishedCallback;
import com.snowflake.connectors.taskreactor.worker.ingestion.Ingestion;
import com.snowflake.connectors.taskreactor.worker.queue.WorkItem;
import com.snowflake.snowpark_java.Session;

public class GitLabIngestion implements Ingestion<IngestionRun.IngestionStatus> {

  private final Session session;
  private final IngestionRunRepository ingestionRunRepository;
  private final OnIngestionFinishedCallback onIngestionFinishedCallback;

  private String ingestionRunId;
  private GitLabWorkItem gitLabWorkItem;
  private long ingestedRows = 0L;

  private static final Logger logger = LoggerFactory.getLogger(GitLabIngestion.class);

  public GitLabIngestion(
      Session session,
      IngestionRunRepository ingestionRunRepository,
      OnIngestionFinishedCallback onIngestionFinishedCallback) {
    this.session = session;
    this.ingestionRunRepository = ingestionRunRepository;
    this.onIngestionFinishedCallback = onIngestionFinishedCallback;
  }

  @Override
  public IngestionRun.IngestionStatus initialState(WorkItem workItem) {
    this.gitLabWorkItem = GitLabWorkItem.from(workItem);
    return IN_PROGRESS;
  }

  @Override
  public void preIngestion(WorkItem workItem) {
    this.ingestionRunId =
        ingestionRunRepository.startRun(
            gitLabWorkItem.getResourceIngestionDefinitionId(),
            gitLabWorkItem.getIngestionConfigurationId(),
            gitLabWorkItem.getProcessId(),
            null);
  }

  @Override
  public IngestionRun.IngestionStatus performIngestion(
      WorkItem workItem, IngestionRun.IngestionStatus ingestionStatus) {
    if (gitLabWorkItem.isInstanceType()) {
      return performIngestionForInstance();
    } else if (gitLabWorkItem.isGroupType()) {
      return performIngestionForGroup();
    } else if (gitLabWorkItem.isProjectType()) {
      return performIngestionForProject();
    } else {
      return FAILED;
    }
  }

  @Override
  public boolean isIngestionCompleted(WorkItem workItem, IngestionRun.IngestionStatus status) {
    return status != IN_PROGRESS;
  }

  @Override
  public void postIngestion(WorkItem workItem, IngestionRun.IngestionStatus ingestionStatus) {
    ingestionRunRepository.endRun(ingestionRunId, ingestionStatus, ingestedRows, null);
    onIngestionFinishedCallback.onIngestionFinished(gitLabWorkItem.getProcessId(), null);
  }

  @Override
  public void ingestionCancelled(WorkItem workItem, IngestionRun.IngestionStatus lastState) {
    ingestionRunRepository.endRun(ingestionRunId, CANCELED, ingestedRows, null);
  }

  private IngestionRun.IngestionStatus performIngestionForInstance() {
    boolean hasFailures = false;

    try {
      this.ingestedRows = IngestAuditEventsService.execute(session);
    } catch (Exception exception) {
      hasFailures = true;
      logger.error("Exception occurred during instance audit events ingestion", exception);
    }

    try {
      this.ingestedRows += IngestUsersService.execute(session);
    } catch (Exception exception) {
      hasFailures = true;
      logger.error("Exception occurred during instasnce users audit events ingestion", exception);
    }

    try {
      this.ingestedRows += IngestProjectsService.execute(session);
    } catch (Exception exception) {
      hasFailures = true;
      logger.error("Exception occurred during instance projects audit events ingestion", exception);
    }

    if (this.ingestedRows > 0) {
      return COMPLETED;
    }
    return hasFailures ? FAILED : COMPLETED;
  }

  private IngestionRun.IngestionStatus performIngestionForGroup() {
    try {
      this.ingestedRows = IngestGroupAuditEventsService.execute(session, gitLabWorkItem);
      return COMPLETED;
    } catch (Exception exception) {
      logger.error("Exception occurred during group audit events ingestion", exception);
      return FAILED;
    }
  }

  private IngestionRun.IngestionStatus performIngestionForProject() {
    try {
      this.ingestedRows = IngestProjectAuditEventsService.execute(session, gitLabWorkItem);
      return COMPLETED;
    } catch (Exception exception) {
      return FAILED;
    }
  }
}
