package com.gitlab.connectors.ingestion.utils;

import static com.gitlab.connectors.configuration.connection.GitlabConnectionConfiguration.GITLAB_INSTANCE_URL;
import static com.snowflake.connectors.application.configuration.connector.ConnectorConfigurationKey.DESTINATION_DATABASE;
import static com.snowflake.connectors.application.configuration.connector.ConnectorConfigurationKey.DESTINATION_SCHEMA;

import com.gitlab.connectors.configuration.utils.Configuration;
import com.gitlab.connectors.configuration.utils.ConnectorConfigurationPropertyNotFoundException;
import com.snowflake.connectors.common.object.ObjectName;
import com.snowflake.snowpark_java.Session;

public class Ingestion {
  public static ObjectName getDatabaseTableObjectName(Session session, String tableName) {
    Configuration connectorConfig = Configuration.fromConnectorConfig(session);
    String destinationDatabase =
        connectorConfig
            .getValue(DESTINATION_DATABASE.getPropertyName())
            .orElseThrow(
                () ->
                    new ConnectorConfigurationPropertyNotFoundException(
                        DESTINATION_DATABASE.getPropertyName()));

    String destinationSchema =
        connectorConfig
            .getValue(DESTINATION_SCHEMA.getPropertyName())
            .orElseThrow(
                () ->
                    new ConnectorConfigurationPropertyNotFoundException(
                        DESTINATION_SCHEMA.getPropertyName()));

    return ObjectName.from(destinationDatabase, destinationSchema, tableName);
  }

  public static boolean isGitLabDotCom(Session session) {
    String gitlabInstanceUrl =
        Configuration.fromConnectionConfig(session).getValue(GITLAB_INSTANCE_URL).get();
    return gitlabInstanceUrl.equalsIgnoreCase("gitlab.com");
  }
}
