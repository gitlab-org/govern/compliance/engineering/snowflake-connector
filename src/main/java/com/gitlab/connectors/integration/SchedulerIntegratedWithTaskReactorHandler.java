package com.gitlab.connectors.integration;

import com.snowflake.connectors.application.integration.SchedulerTaskReactorOnIngestionScheduled;
import com.snowflake.connectors.application.scheduler.RunSchedulerIterationHandler;
import com.snowflake.connectors.common.object.Identifier;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class SchedulerIntegratedWithTaskReactorHandler {

  public static Variant runIteration(Session session) {
    var callback =
        SchedulerTaskReactorOnIngestionScheduled.getInstance(
            session, Identifier.from("GITLAB_CONNECTOR_TASK_REACTOR"));
    return RunSchedulerIterationHandler.builder(session)
        .withCallback(callback)
        .build()
        .runIteration()
        .toVariant();
  }
}
