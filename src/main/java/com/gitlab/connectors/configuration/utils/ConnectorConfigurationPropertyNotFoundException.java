package com.gitlab.connectors.configuration.utils;

public class ConnectorConfigurationPropertyNotFoundException extends RuntimeException {

  private static final String ERROR_MESSAGE =
      "Property [%s] not found in the Connector Configuration";

  public ConnectorConfigurationPropertyNotFoundException(String property) {
    super(String.format(ERROR_MESSAGE, property));
  }
}
