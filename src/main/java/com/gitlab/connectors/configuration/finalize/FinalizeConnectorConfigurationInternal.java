package com.gitlab.connectors.configuration.finalize;

import static com.gitlab.connectors.ConnectorObjects.*;
import static com.snowflake.connectors.application.configuration.connector.ConnectorConfigurationKey.DESTINATION_DATABASE;
import static com.snowflake.connectors.application.configuration.connector.ConnectorConfigurationKey.DESTINATION_SCHEMA;
import static com.snowflake.connectors.util.sql.SqlTools.callProcedure;
import static com.snowflake.connectors.util.sql.SqlTools.varcharArgument;
import static java.lang.String.format;

import java.util.Optional;

import com.gitlab.connectors.configuration.utils.Configuration;
import com.snowflake.connectors.application.configuration.finalization.FinalizeConnectorCallback;
import com.snowflake.connectors.application.scheduler.SchedulerCreator;
import com.snowflake.connectors.common.object.ObjectName;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class FinalizeConnectorConfigurationInternal implements FinalizeConnectorCallback {
  private static final String WAREHOUSE_REFERENCE = "reference(\\'WAREHOUSE_REFERENCE\\')";
  private static final String NULL_ARG = "null";

  private final Session session;
  private final SchedulerCreator schedulerCreator;

  public FinalizeConnectorConfigurationInternal(
      Session session, SchedulerCreator schedulerCreator) {
    this.session = session;
    this.schedulerCreator = schedulerCreator;
  }

  @Override
  public ConnectorResponse execute(Variant variant) {
    Configuration connectorConfig = Configuration.fromConnectorConfig(session);
    Optional<String> destinationDatabase =
        connectorConfig.getValue(DESTINATION_DATABASE.getPropertyName());
    Optional<String> destinationSchema =
        connectorConfig.getValue(DESTINATION_SCHEMA.getPropertyName());

    if (destinationDatabase.isEmpty()) {
      return Configuration.keyNotFoundResponse(DESTINATION_DATABASE.getPropertyName());
    }
    if (destinationSchema.isEmpty()) {
      return Configuration.keyNotFoundResponse(DESTINATION_SCHEMA.getPropertyName());
    }
    createDestinationDbObjects(destinationDatabase.get(), destinationSchema.get());
    createRequiredTables(destinationDatabase.get(), destinationSchema.get());
    createRequiredViews(destinationDatabase.get(), destinationSchema.get());
    initializeTaskReactorInstance();
    initializeScheduler();
    setTaskReactorWorkersNumber();
    return ConnectorResponse.success();
  }

  private void createDestinationDbObjects(String destinationDatabase, String destinationSchema) {
    session.sql(format("CREATE DATABASE IF NOT EXISTS %s", destinationDatabase)).collect();
    session
        .sql(format("GRANT USAGE ON DATABASE %s TO APPLICATION ROLE ADMIN", destinationDatabase))
        .collect();
    session
        .sql(format("CREATE SCHEMA IF NOT EXISTS %s.%s", destinationDatabase, destinationSchema))
        .collect();
    session
        .sql(
            format(
                "GRANT USAGE ON SCHEMA %s.%s TO APPLICATION ROLE ADMIN",
                destinationDatabase, destinationSchema))
        .collect();
  }

  private void createRequiredTables(String database, String schema) {
    createInstanceAuditEventsTable(database, schema);
    createGroupAuditEventsTable(database, schema);
    createProjectAuditEventsTable(database, schema);
    createUsersTable(database, schema);
    createProjectsTable(database, schema);
  }

  private void createRequiredViews(String database, String schema) {
    createInstanceAuditEventsView(database, schema);
    createGroupAuditEventsView(database, schema);
    createProjectAuditEventsView(database, schema);
    createUsersView(database, schema);
    createProjectsView(database, schema);
  }

  private void createInstanceAuditEventsTable(String database, String schema) {
    String tableName =
        ObjectName.from(database, schema, INSTANCE_AUDIT_EVENTS_TABLE).getEscapedName();
    session.sql(format("CREATE TABLE IF NOT EXISTS %s (RAW_DATA VARIANT)", tableName)).collect();
    session.sql(format("GRANT ALL ON TABLE %s TO APPLICATION ROLE ADMIN", tableName)).collect();
    session
        .sql(format("GRANT SELECT ON TABLE %s TO APPLICATION ROLE DATA_READER", tableName))
        .collect();
  }

  private void createGroupAuditEventsTable(String database, String schema) {
    String tableName = ObjectName.from(database, schema, GROUP_AUDIT_EVENTS_TABLE).getEscapedName();
    session
        .sql(
            format(
                "CREATE TABLE IF NOT EXISTS %s (GROUP_NAME VARCHAR, RAW_DATA VARIANT)", tableName))
        .collect();
    session.sql(format("GRANT ALL ON TABLE %s TO APPLICATION ROLE ADMIN", tableName)).collect();
    session
        .sql(format("GRANT SELECT ON TABLE %s TO APPLICATION ROLE DATA_READER", tableName))
        .collect();
  }

  private void createProjectAuditEventsTable(String database, String schema) {
    String tableName =
        ObjectName.from(database, schema, PROJECT_AUDIT_EVENTS_TABLE).getEscapedName();
    session
        .sql(
            format(
                "CREATE TABLE IF NOT EXISTS %s (PROJECT_NAME VARCHAR, RAW_DATA VARIANT)",
                tableName))
        .collect();
    session.sql(format("GRANT ALL ON TABLE %s TO APPLICATION ROLE ADMIN", tableName)).collect();
    session
        .sql(format("GRANT SELECT ON TABLE %s TO APPLICATION ROLE DATA_READER", tableName))
        .collect();
  }

  private void createUsersTable(String database, String schema) {
    String tableName = ObjectName.from(database, schema, USERS_TABLE).getEscapedName();
    session.sql(format("CREATE TABLE IF NOT EXISTS %s (RAW_DATA VARIANT)", tableName)).collect();
    session.sql(format("GRANT ALL ON TABLE %s TO APPLICATION ROLE ADMIN", tableName)).collect();
    session
        .sql(format("GRANT SELECT ON TABLE %s TO APPLICATION ROLE DATA_READER", tableName))
        .collect();
  }

  private void createProjectsTable(String database, String schema) {
    String tableName = ObjectName.from(database, schema, PROJECTS_TABLE).getEscapedName();
    session.sql(format("CREATE TABLE IF NOT EXISTS %s (RAW_DATA VARIANT)", tableName)).collect();
    session.sql(format("GRANT ALL ON TABLE %s TO APPLICATION ROLE ADMIN", tableName)).collect();
    session
        .sql(format("GRANT SELECT ON TABLE %s TO APPLICATION ROLE DATA_READER", tableName))
        .collect();
  }

  private void createInstanceAuditEventsView(String database, String schema) {
    String tableName =
        ObjectName.from(database, schema, INSTANCE_AUDIT_EVENTS_TABLE).getEscapedName();
    String viewName =
        ObjectName.from(database, schema, INSTANCE_AUDIT_EVENTS_VIEW).getEscapedName();

    session
        .sql(
            format(
                "CREATE VIEW IF NOT EXISTS %s AS ( "
                    + " SELECT RAW_DATA['id'] as id,\n"
                    + " RAW_DATA['created_at'] as created_at,\n"
                    + " RAW_DATA['entity_type'] as entity_type,\n"
                    + " RAW_DATA['entity_id'] as entity_id,\n"
                    + " RAW_DATA['author_id'] as author_id,\n"
                    + " RAW_DATA['details']['author_name'] as author_name,\n"
                    + " RAW_DATA['details']['author_email'] as author_email,\n"
                    + " RAW_DATA['details']['target_id'] as target_id,\n"
                    + " RAW_DATA['details']['target_type'] as target_type,\n"
                    + " RAW_DATA['details']['target_details'] as target_details,\n"
                    + " RAW_DATA['details']['custom_message'] as custom_message,\n"
                    + " RAW_DATA['details']['ip_address'] as ip_address,\n"
                    + " RAW_DATA['details'] as details\n"
                    + " from %s "
                    + " )",
                viewName, tableName))
        .collect();

    session.sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE ADMIN", viewName)).collect();
    session
        .sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE DATA_READER", viewName))
        .collect();
  }

  private void createGroupAuditEventsView(String database, String schema) {
    String tableName = ObjectName.from(database, schema, GROUP_AUDIT_EVENTS_TABLE).getEscapedName();
    String viewName = ObjectName.from(database, schema, GROUP_AUDIT_EVENTS_VIEW).getEscapedName();

    session
        .sql(
            format(
                "CREATE VIEW IF NOT EXISTS %s AS ( "
                    + " SELECT RAW_DATA['id'] as id,\n"
                    + " GROUP_NAME,\n"
                    + " RAW_DATA['created_at'] as created_at,\n"
                    + " RAW_DATA['entity_type'] as entity_type,\n"
                    + " RAW_DATA['entity_id'] as entity_id,\n"
                    + " RAW_DATA['author_id'] as author_id,\n"
                    + " RAW_DATA['details']['author_name'] as author_name,\n"
                    + " RAW_DATA['details']['author_email'] as author_email,\n"
                    + " RAW_DATA['details']['target_id'] as target_id,\n"
                    + " RAW_DATA['details']['target_type'] as target_type,\n"
                    + " RAW_DATA['details']['target_details'] as target_details,\n"
                    + " RAW_DATA['details']['custom_message'] as custom_message,\n"
                    + " RAW_DATA['details']['ip_address'] as ip_address,\n"
                    + " RAW_DATA['details'] as details\n"
                    + " from %s "
                    + " )",
                viewName, tableName))
        .collect();

    session.sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE ADMIN", viewName)).collect();
    session
        .sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE DATA_READER", viewName))
        .collect();
  }

  private void createProjectAuditEventsView(String database, String schema) {
    String tableName =
        ObjectName.from(database, schema, PROJECT_AUDIT_EVENTS_TABLE).getEscapedName();
    String viewName = ObjectName.from(database, schema, PROJECT_AUDIT_EVENTS_VIEW).getEscapedName();

    session
        .sql(
            format(
                "CREATE VIEW IF NOT EXISTS %s AS ( "
                    + " SELECT RAW_DATA['id'] as id,\n"
                    + " PROJECT_NAME,\n"
                    + " RAW_DATA['created_at'] as created_at,\n"
                    + " RAW_DATA['entity_type'] as entity_type,\n"
                    + " RAW_DATA['entity_id'] as entity_id,\n"
                    + " RAW_DATA['author_id'] as author_id,\n"
                    + " RAW_DATA['details']['author_name'] as author_name,\n"
                    + " RAW_DATA['details']['author_email'] as author_email,\n"
                    + " RAW_DATA['details']['target_id'] as target_id,\n"
                    + " RAW_DATA['details']['target_type'] as target_type,\n"
                    + " RAW_DATA['details']['target_details'] as target_details,\n"
                    + " RAW_DATA['details']['custom_message'] as custom_message,\n"
                    + " RAW_DATA['details']['ip_address'] as ip_address,\n"
                    + " RAW_DATA['details'] as details\n"
                    + " from %s "
                    + " )",
                viewName, tableName))
        .collect();

    session.sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE ADMIN", viewName)).collect();
    session
        .sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE DATA_READER", viewName))
        .collect();
  }

  private void createUsersView(String database, String schema) {
    String tableName = ObjectName.from(database, schema, USERS_TABLE).getEscapedName();
    String viewName = ObjectName.from(database, schema, USERS_VIEW).getEscapedName();

    session
        .sql(
            format(
                "CREATE VIEW IF NOT EXISTS %s AS ( "
                    + " SELECT RAW_DATA['id'] as id,\n"
                    + " RAW_DATA['username'] as username,\n"
                    + " RAW_DATA['name'] as name,\n"
                    + " RAW_DATA['state'] as state,\n"
                    + " RAW_DATA['avatar_url'] as avatar_url,\n"
                    + " RAW_DATA['web_url'] as web_url,\n"
                    + " RAW_DATA['created_at'] as created_at,\n"
                    + " RAW_DATA['bio'] as bio,\n"
                    + " RAW_DATA['location'] as location,\n"
                    + " RAW_DATA['public_email'] as public_email,\n"
                    + " RAW_DATA['skype'] as skype,\n"
                    + " RAW_DATA['linkedin'] as linkedin,\n"
                    + " RAW_DATA['twitter'] as twitter,\n"
                    + " RAW_DATA['discord'] as discord,\n"
                    + " RAW_DATA['website_url'] as website_url,\n"
                    + " RAW_DATA['organization'] as organization,\n"
                    + " RAW_DATA['job_title'] as job_title,\n"
                    + " RAW_DATA['pronouns'] as pronouns,\n"
                    + " RAW_DATA['bot'] as bot,\n"
                    + " RAW_DATA['work_information'] as work_information,\n"
                    + " RAW_DATA['followers'] as total_followers,\n"
                    + " RAW_DATA['following'] as total_following,\n"
                    + " RAW_DATA['is_followed'] as is_followed,\n"
                    + " RAW_DATA['local_time'] as local_time,\n"
                    + " RAW_DATA['last_sign_in_at'] as last_sign_in_at,\n"
                    + " RAW_DATA['email'] as email,\n"
                    + " RAW_DATA['theme_id'] as theme_id,\n"
                    + " RAW_DATA['color_scheme_id'] as color_scheme_id,\n"
                    + " RAW_DATA['projects_limit'] as projects_limit,\n"
                    + " RAW_DATA['current_sign_in_at'] as current_sign_in_at,\n"
                    + " RAW_DATA['identities'] as identities,\n"
                    + " RAW_DATA['can_create_group'] as can_create_group,\n"
                    + " RAW_DATA['can_create_project'] as can_create_project,\n"
                    + " RAW_DATA['two_factor_enabled'] as two_factor_enabled,\n"
                    + " RAW_DATA['external'] as external,\n"
                    + " RAW_DATA['private_profile'] as private_profile,\n"
                    + " RAW_DATA['commit_email'] as commit_email,\n"
                    + " RAW_DATA['shared_runners_minutes_limit'] as shared_runners_minutes_limit,\n"
                    + " RAW_DATA['extra_shared_runners_minutes_limit'] as extra_shared_runners_minutes_limit,\n"
                    + " RAW_DATA['scim_identities'] as scim_identities,\n"
                    + " RAW_DATA['is_admin'] as is_admin,\n"
                    + " RAW_DATA['note'] as note,\n"
                    + " RAW_DATA['namespace_id'] as namespace_id,\n"
                    + " RAW_DATA['created_by']['id'] as created_by_id,\n"
                    + " RAW_DATA['created_by']['username'] as created_by_username,\n"
                    + " RAW_DATA['created_by']['name'] as created_by_name,\n"
                    + " RAW_DATA['created_by']['avatar_url'] as created_by_avatar_url,\n"
                    + " RAW_DATA['created_by']['web_url'] as created_by_web_url,\n"
                    + " RAW_DATA['email_reset_offered_at'] as email_reset_offered_at,\n"
                    + " RAW_DATA['using_license_seat'] as using_license_seat,\n"
                    + " RAW_DATA['is_auditor'] as is_auditor,\n"
                    + " RAW_DATA['provisioned_by_group_id'] as provisioned_by_group_id,\n"
                    + " RAW_DATA['enterprise_group_id'] as enterprise_group_id,\n"
                    + " RAW_DATA['enterprise_group_associated_at'] as enterprise_group_associated_at,\n"
                    + " from %s "
                    + " )",
                viewName, tableName))
        .collect();

    session.sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE ADMIN", viewName)).collect();
    session
        .sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE DATA_READER", viewName))
        .collect();
  }

  private void createProjectsView(String database, String schema) {
    String tableName = ObjectName.from(database, schema, PROJECTS_TABLE).getEscapedName();
    String viewName = ObjectName.from(database, schema, PROJECTS_VIEW).getEscapedName();

    session
        .sql(
            format(
                "CREATE VIEW IF NOT EXISTS %s AS ("
                    + " SELECT RAW_DATA['id'] as id,\n"
                    + " RAW_DATA['description'] as description,\n"
                    + " RAW_DATA['name'] as name,\n"
                    + " RAW_DATA['name_with_namespace'] as name_with_namespace,\n"
                    + " RAW_DATA['path'] as path,\n"
                    + " RAW_DATA['path_with_namespace'] as path_with_namespace,\n"
                    + " RAW_DATA['created_at'] as created_at,\n"
                    + " RAW_DATA['default_branch'] as default_branch,\n"
                    + " RAW_DATA['tag_list'] as tag_list,\n"
                    + " RAW_DATA['topics'] as topics,\n"
                    + " RAW_DATA['ssh_url_to_repo'] as ssh_url_to_repo,\n"
                    + " RAW_DATA['http_url_to_repo'] as http_url_to_repo,\n"
                    + " RAW_DATA['web_url'] as web_url,\n"
                    + " RAW_DATA['readme_url'] as readme_url,\n"
                    + " RAW_DATA['forks_count'] as forks_count,\n"
                    + " RAW_DATA['avatar_url'] as avatar_url,\n"
                    + " RAW_DATA['star_count'] as star_count,\n"
                    + " RAW_DATA['last_activity_at'] as last_activity_at,\n"
                    + " RAW_DATA['namespace'] as namespace,\n"
                    + " RAW_DATA['container_registry_image_prefix'] as container_registry_image_prefix,\n"
                    + " RAW_DATA['_links'] as _links,\n"
                    + " RAW_DATA['packages_enabled'] as packages_enabled,\n"
                    + " RAW_DATA['empty_repo'] as empty_repo,\n"
                    + " RAW_DATA['archived'] as archived,\n"
                    + " RAW_DATA['visibility'] as visibility,\n"
                    + " RAW_DATA['owner'] as owner,\n"
                    + " RAW_DATA['resolve_outdated_diff_discussions'] as resolve_outdated_diff_discussions,\n"
                    + " RAW_DATA['container_expiration_policy'] as container_expiration_policy,\n"
                    + " RAW_DATA['repository_object_format'] as repository_object_format,\n"
                    + " RAW_DATA['issues_enabled'] as issues_enabled,\n"
                    + " RAW_DATA['merge_requests_enabled'] as merge_requests_enabled,\n"
                    + " RAW_DATA['wiki_enabled'] as wiki_enabled,\n"
                    + " RAW_DATA['jobs_enabled'] as jobs_enabled,\n"
                    + " RAW_DATA['snippets_enabled'] as snippets_enabled,\n"
                    + " RAW_DATA['container_registry_enabled'] as container_registry_enabled,\n"
                    + " RAW_DATA['service_desk_enabled'] as service_desk_enabled,\n"
                    + " RAW_DATA['can_create_merge_request_in'] as can_create_merge_request_in,\n"
                    + " RAW_DATA['issues_access_level'] as issues_access_level,\n"
                    + " RAW_DATA['repository_access_level'] as repository_access_level,\n"
                    + " RAW_DATA['merge_requests_access_level'] as merge_requests_access_level,\n"
                    + " RAW_DATA['forking_access_level'] as forking_access_level,\n"
                    + " RAW_DATA['wiki_access_level'] as wiki_access_level,\n"
                    + " RAW_DATA['builds_access_level'] as builds_access_level,\n"
                    + " RAW_DATA['snippets_access_level'] as snippets_access_level,\n"
                    + " RAW_DATA['pages_access_level'] as pages_access_level,\n"
                    + " RAW_DATA['analytics_access_level'] as analytics_access_level,\n"
                    + " RAW_DATA['container_registry_access_level'] as container_registry_access_level,\n"
                    + " RAW_DATA['security_and_compliance_access_level'] as security_and_compliance_access_level,\n"
                    + " RAW_DATA['releases_access_level'] as releases_access_level,\n"
                    + " RAW_DATA['environments_access_level'] as environments_access_level,\n"
                    + " RAW_DATA['feature_flags_access_level'] as feature_flags_access_level,\n"
                    + " RAW_DATA['infrastructure_access_level'] as infrastructure_access_level,\n"
                    + " RAW_DATA['monitor_access_level'] as monitor_access_level,\n"
                    + " RAW_DATA['model_experiments_access_level'] as model_experiments_access_level,\n"
                    + " RAW_DATA['model_registry_access_level'] as model_registry_access_level,\n"
                    + " RAW_DATA['emails_disabled'] as emails_disabled,\n"
                    + " RAW_DATA['emails_enabled'] as emails_enabled,\n"
                    + " RAW_DATA['shared_runners_enabled'] as shared_runners_enabled,\n"
                    + " RAW_DATA['lfs_enabled'] as lfs_enabled,\n"
                    + " RAW_DATA['creator_id'] as creator_id,\n"
                    + " RAW_DATA['import_status'] as import_status,\n"
                    + " RAW_DATA['open_issues_count'] as open_issues_count,\n"
                    + " RAW_DATA['description_html'] as description_html,\n"
                    + " RAW_DATA['updated_at'] as updated_at,\n"
                    + " RAW_DATA['ci_config_path'] as ci_config_path,\n"
                    + " RAW_DATA['public_jobs'] as public_jobs,\n"
                    + " RAW_DATA['shared_with_groups'] as shared_with_groups,\n"
                    + " RAW_DATA['only_allow_merge_if_pipeline_succeeds'] as only_allow_merge_if_pipeline_succeeds,\n"
                    + " RAW_DATA['allow_merge_on_skipped_pipeline'] as allow_merge_on_skipped_pipeline,\n"
                    + " RAW_DATA['request_access_enabled'] as request_access_enabled,\n"
                    + " RAW_DATA['only_allow_merge_if_all_discussions_are_resolved'] as only_allow_merge_if_all_discussions_are_resolved,\n"
                    + " RAW_DATA['remove_source_branch_after_merge'] as remove_source_branch_after_merge,\n"
                    + " RAW_DATA['printing_merge_request_link_enabled'] as printing_merge_request_link_enabled,\n"
                    + " RAW_DATA['merge_method'] as merge_method,\n"
                    + " RAW_DATA['squash_option'] as squash_option,\n"
                    + " RAW_DATA['enforce_auth_checks_on_uploads'] as enforce_auth_checks_on_uploads,\n"
                    + " RAW_DATA['suggestion_commit_message'] as suggestion_commit_message,\n"
                    + " RAW_DATA['merge_commit_template'] as merge_commit_template,\n"
                    + " RAW_DATA['squash_commit_template'] as squash_commit_template,\n"
                    + " RAW_DATA['issue_branch_template'] as issue_branch_template,\n"
                    + " RAW_DATA['warn_about_potentially_unwanted_characters'] as warn_about_potentially_unwanted_characters,\n"
                    + " RAW_DATA['autoclose_referenced_issues'] as autoclose_referenced_issues,\n"
                    + " RAW_DATA['external_authorization_classification_label'] as external_authorization_classification_label,\n"
                    + " RAW_DATA['requirements_enabled'] as requirements_enabled,\n"
                    + " RAW_DATA['requirements_access_level'] as requirements_access_level,\n"
                    + " RAW_DATA['security_and_compliance_enabled'] as security_and_compliance_enabled,\n"
                    + " RAW_DATA['compliance_frameworks'] as compliance_frameworks,\n"
                    + " RAW_DATA['permissions'] as permissions,\n"
                    + " from %s "
                    + " )",
                viewName, tableName))
        .collect();

    session.sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE ADMIN", viewName)).collect();
    session
        .sql(format("GRANT SELECT ON VIEW %s TO APPLICATION ROLE DATA_READER", viewName))
        .collect();
  }

  private void initializeTaskReactorInstance() {
    callProcedure(
        session,
        TASK_REACTOR_SCHEMA,
        INITIALIZE_INSTANCE_PROCEDURE,
        varcharArgument(TASK_REACTOR_INSTANCE),
        varcharArgument(WAREHOUSE_REFERENCE),
        NULL_ARG,
        NULL_ARG,
        NULL_ARG,
        NULL_ARG);
  }

  private void initializeScheduler() {
    schedulerCreator.createScheduler();
  }

  private void setTaskReactorWorkersNumber() {
    session
        .sql(
            String.format(
                "CALL %s.%s(%d, '%s')",
                TASK_REACTOR_SCHEMA, SET_WORKERS_NUMBER_PROCEDURE, 5, TASK_REACTOR_INSTANCE))
        .collect();
  }
}
