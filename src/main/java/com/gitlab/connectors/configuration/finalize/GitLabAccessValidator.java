package com.gitlab.connectors.configuration.finalize;

import static java.lang.String.format;

import java.net.http.HttpResponse;
import java.util.Optional;

import com.gitlab.connectors.configuration.utils.Configuration;
import com.gitlab.connectors.http.GitlabHttpHelper;
import com.snowflake.connectors.application.configuration.finalization.SourceValidator;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class GitLabAccessValidator implements SourceValidator {
  private static final String GROUP_NAME_KEY = "group_name";
  private static final String GROUP_NOT_FOUND_ERROR = "GROUP_NOT_FOUND";
  private static final String GROUP_NOT_FOUND_MSG =
      "Group does not exist or insufficient privileges granted to personal access token.";
  private static final String INVALID_ACCESS_TOKEN_ERROR = "INVALID_ACCESS_TOKEN";
  private static final String INVALID_ACCESS_TOKEN_MSG =
      "GitLab Access Token stored in connector configuration is invalid. Make sure the token has"
          + " not expired and repeat Connection Configuration step.";
  private static final String UNHANDLED_RESPONSE_CODE_ERROR = "UNHANDLED_RESPONSE_CODE";
  private static final String UNHANDLED_RESPONSE_CODE_MSG =
      "Response code [%s] received in response from GitLab API is unhandled by the connector.";
  private final Session session;

  public GitLabAccessValidator(Session session) {
    this.session = session;
  }

  @Override
  public ConnectorResponse validate(Variant variant) {
    Configuration finalizeProperties = Configuration.fromCustomConfig(variant);
    Optional<String> groupName = finalizeProperties.getValue(GROUP_NAME_KEY);
    if (groupName.isEmpty()) {
      return Configuration.keyNotFoundResponse(GROUP_NAME_KEY);
    }
    HttpResponse<String> httpResponse =
        new GitlabHttpHelper(session).groupAuditEvents(groupName.get());
    return prepareConnectorResponse(httpResponse.statusCode());
  }

  private ConnectorResponse prepareConnectorResponse(int statusCode) {
    switch (statusCode) {
      case 200:
        return ConnectorResponse.success();
      case 401:
        return ConnectorResponse.error(INVALID_ACCESS_TOKEN_ERROR, INVALID_ACCESS_TOKEN_MSG);
      case 404:
        return ConnectorResponse.error(GROUP_NOT_FOUND_ERROR, GROUP_NOT_FOUND_MSG);
      default:
        return ConnectorResponse.error(
            UNHANDLED_RESPONSE_CODE_ERROR, format(UNHANDLED_RESPONSE_CODE_MSG, statusCode));
    }
  }
}
