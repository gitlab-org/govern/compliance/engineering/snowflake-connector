package com.gitlab.connectors.configuration.finalize;

import com.snowflake.connectors.application.configuration.finalization.FinalizeConnectorHandler;
import com.snowflake.connectors.application.scheduler.SchedulerCreator;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class FinalizeConnectorConfigurationCustomHandler {
  public Variant finalizeConnectorConfiguration(Session session, Variant customConfig) {
    SchedulerCreator schedulerCreator = SchedulerCreator.getInstance(session);
    FinalizeConnectorHandler handler =
        FinalizeConnectorHandler.builder(session)
            .withCallback(new FinalizeConnectorConfigurationInternal(session, schedulerCreator))
            .withSourceValidator(new GitLabAccessValidator(session))
            .withInputValidator(v -> ConnectorResponse.success())
            .build();
    return handler.finalizeConnectorConfiguration(customConfig).toVariant();
  }
}
