package com.gitlab.connectors.configuration.connection;

public class GitlabConnectionConfiguration {

  public static final String INTEGRATION_PARAM = "external_access_integration";
  public static final String SECRET_PARAM = "secret";
  public static final String TOKEN_NAME = "gitlab_personal_access_token";
  public static final String GITLAB_INSTANCE_URL = "gitlab_instance_url";

  private GitlabConnectionConfiguration() {}
}
