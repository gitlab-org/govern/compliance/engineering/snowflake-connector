package com.gitlab.connectors.configuration.connection;

import static java.lang.String.format;

import java.util.Optional;

import com.gitlab.connectors.configuration.utils.Configuration;
import com.snowflake.connectors.application.configuration.connection.ConnectionConfigurationInputValidator;
import com.snowflake.connectors.common.exception.ConnectorException;
import com.snowflake.connectors.common.object.ObjectName;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.snowpark_java.types.Variant;

public class GitlabConnectionConfigurationInputValidator
    implements ConnectionConfigurationInputValidator {

  private static final String ERROR_CODE = "INVALID_CONNECTION_CONFIGURATION";

  @Override
  public ConnectorResponse validate(Variant config) {
    var integrationCheck =
        checkParameter(config, GitlabConnectionConfiguration.INTEGRATION_PARAM, false);
    if (!integrationCheck.isOk()) {
      return integrationCheck;
    }

    var secretCheck = checkParameter(config, GitlabConnectionConfiguration.SECRET_PARAM, true);
    if (!secretCheck.isOk()) {
      return secretCheck;
    }

    var gitlabInstanceUrlCheck =
        checkParameter(config, GitlabConnectionConfiguration.GITLAB_INSTANCE_URL, false);
    if (!gitlabInstanceUrlCheck.isOk()) {
      return gitlabInstanceUrlCheck;
    }

    return ConnectorResponse.success();
  }

  private ConnectorResponse checkParameter(
      Variant config, String parameterName, boolean fullyQualified) {
    Configuration connectorConfig = Configuration.fromCustomConfig(config);
    Optional<String> value = connectorConfig.getValue(parameterName);

    if (value.isEmpty()) {
      return ConnectorResponse.error(
          ERROR_CODE, format("Missing configuration parameter: %s", parameterName));
    }

    try {
      var objectName = ObjectName.fromString(value.get());
      if (fullyQualified && !objectName.isFullyQualified()) {
        return ConnectorResponse.error(
            ERROR_CODE, format("Parameter %s is not a fully qualified object name", parameterName));
      }

      return ConnectorResponse.success();
    } catch (ConnectorException exception) {
      return exception.getResponse();
    }
  }
}
