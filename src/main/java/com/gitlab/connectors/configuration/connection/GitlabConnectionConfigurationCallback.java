package com.gitlab.connectors.configuration.connection;

import static com.gitlab.connectors.ConnectorObjects.*;
import static com.snowflake.connectors.util.sql.SqlTools.callProcedure;
import static com.snowflake.connectors.util.sql.SqlTools.varcharArgument;

import com.snowflake.connectors.application.configuration.connection.ConnectionConfigurationCallback;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class GitlabConnectionConfigurationCallback implements ConnectionConfigurationCallback {

  private final Session session;

  public GitlabConnectionConfigurationCallback(Session session) {
    this.session = session;
  }

  @Override
  public ConnectorResponse execute(Variant config) {
    configureProcedure(config);

    return ConnectorResponse.success();
  }

  private void configureProcedure(Variant config) {
    var configMap = config.asMap();

    callProcedure(
        session,
        PUBLIC_SCHEMA,
        "CONFIGURE_PROCEDURES",
        varcharArgument(configMap.get(GitlabConnectionConfiguration.SECRET_PARAM).asString()),
        varcharArgument(configMap.get(GitlabConnectionConfiguration.INTEGRATION_PARAM).asString()));
  }
}
