package com.gitlab.connectors.configuration.connection;

import static com.gitlab.connectors.http.GitlabHttpHelper.isSuccessful;

import java.net.http.HttpResponse;

import com.gitlab.connectors.configuration.utils.Configuration;
import com.gitlab.connectors.http.GitlabHttpHelper;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class GitlabConnectionValidator {
  private static final String ERROR_CODE = "TEST_CONNECTION_FAILED";

  public static Variant testConnection(Session session) {
    Configuration.fromConnectionConfig(session);
    return testProjectsApi(session).toVariant();
  }

  private static ConnectorResponse testProjectsApi(Session session) {
    try {
      HttpResponse<String> response = new GitlabHttpHelper(session).projects();
      if (isSuccessful(response.statusCode())) {
        return ConnectorResponse.success();
      } else {
        return ConnectorResponse.error(
            ERROR_CODE,
            "Test connection to GitLab API failed. The response code "
                + response.statusCode()
                + " is not included in the success response code group.");
      }
    } catch (Exception exception) {
      return ConnectorResponse.error(
          ERROR_CODE, "Test connection to GitLab API failed " + exception.getMessage());
    }
  }
}
