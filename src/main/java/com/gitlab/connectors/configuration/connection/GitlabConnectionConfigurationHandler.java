package com.gitlab.connectors.configuration.connection;

import com.snowflake.connectors.application.configuration.connection.ConnectionConfigurationHandler;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class GitlabConnectionConfigurationHandler {
  public static Variant setConnectionConfiguration(Session session, Variant config) {
    var handler =
        ConnectionConfigurationHandler.builder(session)
            .withInputValidator(new GitlabConnectionConfigurationInputValidator())
            .withCallback(new GitlabConnectionConfigurationCallback(session))
            .build();
    return handler.setConnectionConfiguration(config).toVariant();
  }
}
