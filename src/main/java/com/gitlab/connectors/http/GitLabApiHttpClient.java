package com.gitlab.connectors.http;

import static com.gitlab.connectors.configuration.connection.GitlabConnectionConfiguration.GITLAB_INSTANCE_URL;
import static java.lang.String.format;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import com.gitlab.connectors.configuration.connection.GitlabConnectionConfiguration;
import com.gitlab.connectors.configuration.utils.Configuration;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.SnowflakeSecrets;

public class GitLabApiHttpClient {
  private static final Duration REQUEST_TIMEOUT = Duration.ofSeconds(30);

  private final HttpClient client;
  private final String secret;
  private final String gitlab_instance_url;

  public GitLabApiHttpClient(Session session) {
    this.client = HttpClient.newHttpClient();
    this.secret =
        SnowflakeSecrets.newInstance()
            .getGenericSecretString(GitlabConnectionConfiguration.TOKEN_NAME);
    this.gitlab_instance_url =
        Configuration.fromConnectionConfig(session).getValue(GITLAB_INSTANCE_URL).get();
  }

  /**
   * Perform a GET HTTP request.
   *
   * @param apiPath Target URL
   * @return HTTP response
   */
  public HttpResponse<String> get(String apiPath) {
    var request =
        HttpRequest.newBuilder()
            .uri(URI.create(getFullApiUrl(apiPath)))
            .GET()
            .header("Authorization", format("Bearer %s", secret))
            .header("Content-Type", "application/json")
            .header("User-Agent", "GitLab Data Connector")
            .timeout(REQUEST_TIMEOUT)
            .build();

    try {
      return client.send(request, HttpResponse.BodyHandlers.ofString());
    } catch (IOException | InterruptedException ex) {
      throw new RuntimeException(format("HttpRequest failed: %s", ex.getMessage()), ex);
    }
  }

  private String getFullApiUrl(String apiPath) {
    return "https://" + gitlab_instance_url + "/api/v4" + apiPath;
  }
}
