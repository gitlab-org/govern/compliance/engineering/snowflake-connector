package com.gitlab.connectors.http;

import java.io.IOException;
import java.net.URLEncoder;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class GitlabHttpHelper {
  private final GitLabApiHttpClient gitlabClient;

  public GitlabHttpHelper(Session session) {
    this.gitlabClient = new GitLabApiHttpClient(session);
  }

  public HttpResponse<String> projects() {
    return gitlabClient.get("/projects");
  }

  public HttpResponse<String> groupAuditEvents(String groupName) {
    String urlEncodedGroupName = URLEncoder.encode(groupName, StandardCharsets.UTF_8);
    return gitlabClient.get(String.join("", "/groups/", urlEncodedGroupName, "/audit_events"));
  }

  public List<Variant> fetchGroupAuditEvents(String groupName, String createdAfter) {
    String urlEncodedGroupName = URLEncoder.encode(groupName, StandardCharsets.UTF_8);
    String apiPath =
        String.join(
            "",
            "/groups/",
            urlEncodedGroupName,
            "/audit_events",
            "?pagination=keyset&order_by=id&sort=desc&per_page=100");

    return fetchUsingPagination(apiPath, "created_after", createdAfter);
  }

  public List<Variant> fetchProjectAuditEvents(String projectName, String createdAfter) {
    String urlEncodedProjectName = URLEncoder.encode(projectName, StandardCharsets.UTF_8);
    String apiPath =
        String.join(
            "",
            "/projects/",
            urlEncodedProjectName,
            "/audit_events",
            "?pagination=keyset&order_by=id&sort=desc&per_page=100");

    return fetchUsingPagination(apiPath, "created_after", createdAfter);
  }

  public List<Variant> fetchInstanceAuditEvents(String createdAfter) {
    String apiPath = "/audit_events?pagination=keyset&order_by=id&sort=desc&per_page=100";
    return fetchUsingPagination(apiPath, "created_after", createdAfter);
  }

  public List<Variant> fetchUsers(String createdAfter) {
    String apiPath = String.join("", "/users/?pagination=keyset&order_by=id&sort=asc&per_page=100");
    return fetchUsingPagination(apiPath, "created_after", createdAfter);
  }

  public List<Variant> fetchProjects(String lastId, String lastUpdatedAt) {
    String baseApiPath = "/projects/?pagination=keyset&per_page=100";

    if (lastId != null) {
      // New projects since after last known ID
      return fetchUsingPagination(baseApiPath + "&order_by=id&sort=asc", "id_after", lastId);
    } else if (lastUpdatedAt != null) {
      // Update existing projects
      return fetchUsingPagination(
          baseApiPath + "&order_by=updated_at&sort=asc", "updated_after", lastUpdatedAt);
    } else {
      // For initial run, ingest all projects
      return fetchUsingPagination(baseApiPath + "&order_by=id&sort=asc", null, null);
    }
  }

  private List<Variant> fetchUsingPagination(String apiPath, String filterName, String filterVal) {
    if (filterVal != null && filterName != null) {
      apiPath = String.join("", apiPath, String.format("&%s=", filterName), filterVal);
    }

    List<Variant> allFetchedData = new ArrayList<>();
    String nextPage = "";
    while (nextPage != null) {
      HttpResponse<String> httpResponse = gitlabClient.get(apiPath);
      nextPage = getLinkForNextPage(httpResponse);

      if (nextPage != null) {
        // Extract API path with parameters from the full URL.
        apiPath = nextPage.substring(nextPage.lastIndexOf('/'));
      }

      try {
        allFetchedData.addAll(
            Arrays.stream(new ObjectMapper().readValue(httpResponse.body(), Map[].class))
                .map(Variant::new)
                .collect(Collectors.toList()));
      } catch (IOException e) {
        throw new RuntimeException("Cannot parse JSON", e);
      }

      // Break the loop for the following two conditions
      // 1. When the API is executed first time after installation we don't fetch all
      // the historical events.
      // 2. OR if the total number of rows to ingest reaches 500. This limit can be
      // removed once we resolve
      // https://gitlab.com/gitlab-org/govern/compliance/engineering/snowflake-connector/-/issues/1
      if (filterVal == null || allFetchedData.size() >= 500) {
        break;
      }
    }
    return allFetchedData;
  }

  private String getLinkForNextPage(HttpResponse<String> httpResponse) {
    List<String> linkHeader = httpResponse.headers().allValues("link");
    if (!linkHeader.isEmpty()) {
      String linkHeaderValue = linkHeader.get(0);
      // Example of linkHeaderValue
      // <https://gitlab.com/api/v4/audit_events?created_after=2024-05-07T11%3A15%3A00%2B00%3A00&cursor=eyJpZCI6IjczMjQzNDA4IiwiX2tkIjoibiJ9&order_by=id&page=1&pagination=keyset&per_page=100&sort=desc>; rel="next"
      // Extract the string between the angular brackets.
      return linkHeaderValue.substring(1, linkHeaderValue.indexOf('>'));
    }
    return null;
  }

  public static boolean isSuccessful(int statusCode) {
    return statusCode >= 200 && statusCode <= 299;
  }
}
