package com.gitlab.connectors.lifecycle.pause;

import static com.gitlab.connectors.ConnectorObjects.*;

import com.snowflake.connectors.application.lifecycle.pause.PauseConnectorCallback;
import com.snowflake.connectors.common.object.ObjectName;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.connectors.common.task.TaskRef;
import com.snowflake.connectors.common.task.TaskRepository;

public class InternalPauseConnectorCallback implements PauseConnectorCallback {
  private static final String ERROR_CODE = "PAUSE_CONNECTOR_FAILED";
  private static final String ERROR_MSG = "Unable to suspend all connector tasks";
  private final TaskRepository tasksRepository;

  public InternalPauseConnectorCallback(TaskRepository tasksRepository) {
    this.tasksRepository = tasksRepository;
  }

  @Override
  public ConnectorResponse execute() {
    ObjectName schedulerTaskName = ObjectName.from(STATE_SCHEMA, SCHEDULER_TASK);
    TaskRef schedulerTask = tasksRepository.fetch(schedulerTaskName);
    try {
      schedulerTask.suspend();
    } catch (Exception e) {
      return ConnectorResponse.error(ERROR_CODE, ERROR_MSG);
    }
    return ConnectorResponse.success();
  }
}
