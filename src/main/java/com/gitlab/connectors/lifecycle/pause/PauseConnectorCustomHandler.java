package com.gitlab.connectors.lifecycle.pause;

import com.snowflake.connectors.application.lifecycle.pause.PauseConnectorHandler;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.connectors.common.task.TaskRepository;
import com.snowflake.snowpark_java.Session;
import com.snowflake.snowpark_java.types.Variant;

public class PauseConnectorCustomHandler {
  public Variant pauseConnector(Session session) {
    var internalCallback = new InternalPauseConnectorCallback(TaskRepository.getInstance(session));
    var handler =
        PauseConnectorHandler.builder(session)
            .withStateValidator(ConnectorResponse::success)
            .withCallback(internalCallback)
            .build();
    return handler.pauseConnector().toVariant();
  }
}
