package com.gitlab.connectors.lifecycle.resume;

import static com.gitlab.connectors.ConnectorObjects.*;

import com.snowflake.connectors.application.lifecycle.resume.ResumeConnectorCallback;
import com.snowflake.connectors.common.object.ObjectName;
import com.snowflake.connectors.common.response.ConnectorResponse;
import com.snowflake.connectors.common.task.TaskRef;
import com.snowflake.connectors.common.task.TaskRepository;

public class InternalResumeConnectorCallback implements ResumeConnectorCallback {
  private static final String ERROR_CODE = "RESUME_CONNECTOR_FAILED";
  private static final String ERROR_MSG = "Unable to resume all connector tasks";
  private final TaskRepository taskRepository;

  public InternalResumeConnectorCallback(TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  @Override
  public ConnectorResponse execute() {
    ObjectName schedulerTaskName = ObjectName.from(STATE_SCHEMA, SCHEDULER_TASK);
    TaskRef schedulerTask = taskRepository.fetch(schedulerTaskName);
    try {
      schedulerTask.resume();
    } catch (Exception e) {
      return ConnectorResponse.error(ERROR_CODE, ERROR_MSG);
    }
    return ConnectorResponse.success();
  }
}
