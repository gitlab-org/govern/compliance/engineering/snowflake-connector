package com.gitlab.connectors;

public class ConnectorObjects {

  public static final String INSTANCE_AUDIT_EVENTS_TABLE = "INSTANCE_AUDIT_EVENTS";
  public static final String GROUP_AUDIT_EVENTS_TABLE = "GROUP_AUDIT_EVENTS";
  public static final String PROJECT_AUDIT_EVENTS_TABLE = "PROJECT_AUDIT_EVENTS";
  public static final String USERS_TABLE = "USERS";
  public static final String PROJECTS_TABLE = "PROJECTS";
  public static final String INSTANCE_AUDIT_EVENTS_VIEW = INSTANCE_AUDIT_EVENTS_TABLE + "_VIEW";
  public static final String GROUP_AUDIT_EVENTS_VIEW = GROUP_AUDIT_EVENTS_TABLE + "_VIEW";
  public static final String PROJECT_AUDIT_EVENTS_VIEW = PROJECT_AUDIT_EVENTS_TABLE + "_VIEW";
  public static final String USERS_VIEW = USERS_TABLE + "_VIEW";
  public static final String PROJECTS_VIEW = PROJECTS_TABLE + "_VIEW";
  public static final String SCHEDULER_TASK = "SCHEDULER_TASK";
  public static final String TASK_REACTOR_INSTANCE = "GITLAB_CONNECTOR_TASK_REACTOR";
  public static final String WORKER_PROCEDURE = "GITLAB_WORKER";
  public static final String DISPATCHER_TASK = "DISPATCHER_TASK";
  public static final String TASK_REACTOR_SCHEMA = "TASK_REACTOR";
  public static final String INITIALIZE_INSTANCE_PROCEDURE = "INITIALIZE_INSTANCE";
  public static final String SET_WORKERS_NUMBER_PROCEDURE = "SET_WORKERS_NUMBER";
  public static final String PUBLIC_SCHEMA = "PUBLIC";
  public static final String STATE_SCHEMA = "STATE";
  public static final String TEST_CONNECTION_PROCEDURE = "TEST_CONNECTION";
  public static final String FINALIZE_CONNECTOR_CONFIGURATION_PROCEDURE =
      "FINALIZE_CONNECTOR_CONFIGURATION";

  private ConnectorObjects() {}
}
