# GitLab Snowflake Connector

This app uses [Snowflake Native SDK](https://docs.snowflake.com/developer-guide/native-apps/connector-sdk/about-connector-sdk#what-is-the-native-sdk-for-connectors)
for Connectors which is a library that provides a skeleton for Snowflake native apps whose purpose is to ingest data
from external data source into Snowflake. Snowflake calls such apps as native connectors.

# Development setup

## Prerequisites

1. Install [SnowSQL](https://docs.snowflake.com/en/user-guide/snowsql) CLI. Download installer from [here](https://developers.snowflake.com/snowsql/).
2. Configure SnowSQL config `~/.snowsql/config` with `marketplace_connector_app` as the connection name.
   (SnowSQL will create the necessary config files on first run if they do not yet exist.)

   GitLab team members can get the snowflake account details by reaching out in the
   [`#g_govern_compliance`](https://gitlab.enterprise.slack.com/archives/CN7C8029H) slack channel.

   ```
   [connections.marketplace_connector_app]
   accountname = <>
   username = <>
   password = <>
   rolename = ACCOUNTADMIN
   warehousename = <>
   ```
   - Note: These are all required fields, and all except `warehousename` are from the Snowflake credentials.
   - `warehousename` is your own choice of name, an existing paradigm that exists is: `test_warehouse_{username}`
3. If using `asdf`, run `asdf install` and it will install both Java and Python versions used for this repository, which are defined in `.tool-versions`, otherwise:
  -  Install Java 11. If you are using `asdf` add the java plugin to asdf by running: `asdf plugin add java` then you can install it by running: `asdf install java adoptopenjdk-11.0.22+7`
  - Install Python 3.11.10. This is required by [`snowflake-snowpark-python`](https://github.com/snowflakedb/snowpark-python)
5. Install `snowflake-snowpark-python` by running `pip install snowflake-snowpark-python`.
6. Generate [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with atleast `read_api` access. 

## Setup

1. Run `make complex_create_app_instance_from_app_version` to deploy the GitLab snowflake connector to your Snowflake
   account for testing.
2. After the app is installed we also need to create network rules and grant certain permissions to the app. This can be
   done by logging in via `snowsql` by running `snowsql -c marketplace_connector_app`.
   Run the following commands in `snowsql`:

   (Note: The database GITLAB_SECRETS may already exist and will be shared across other team members. You can skip
   the first row or choose your own database name.)

   ```sql
   CREATE DATABASE GITLAB_SECRETS;

   USE GITLAB_SECRETS;
   CREATE OR REPLACE SECRET GITLAB_PERSONAL_ACCESS_TOKEN_<USERNAME> TYPE=GENERIC_STRING SECRET_STRING='<YOUR-ACCESS-TOKEN>';

   USE GITLAB_CONNECTOR_<USERNAME>;

   CREATE OR REPLACE NETWORK RULE <NAME_OF_YOUR_NETWORK_RULE>
   MODE = EGRESS
   TYPE = HOST_PORT
   VALUE_LIST=('gitlab.com:443', 'staging.gitlab.com:443');

   CREATE OR REPLACE EXTERNAL ACCESS INTEGRATION GITLAB_INTEGRATION_<USERNAME>
   ALLOWED_NETWORK_RULES = (<NAME_OF_YOUR_NETWORK_RULE>)
   ALLOWED_AUTHENTICATION_SECRETS = ('GITLAB_SECRETS.PUBLIC.GITLAB_PERSONAL_ACCESS_TOKEN_<USERNAME>')
   ENABLED = TRUE;

   GRANT USAGE ON INTEGRATION GITLAB_INTEGRATION_<USERNAME> TO APPLICATION GITLAB_CONNECTOR_<USERNAME>_INSTANCE;

   GRANT USAGE ON DATABASE GITLAB_SECRETS TO APPLICATION GITLAB_CONNECTOR_<USERNAME>_INSTANCE;
   GRANT USAGE ON SCHEMA GITLAB_SECRETS.PUBLIC TO APPLICATION GITLAB_CONNECTOR_<USERNAME>_INSTANCE;
   GRANT READ ON SECRET GITLAB_SECRETS.PUBLIC.GITLAB_PERSONAL_ACCESS_TOKEN_<USERNAME> TO APPLICATION GITLAB_CONNECTOR_<USERNAME>_INSTANCE;
   ```
  > Note: Here <USERNAME> refers to the username on your laptop (`whoami`).
- Alternatively, you can run `setup_integration_sql` which will prompt you for the GitLab Personal Access Token and handle the rest.
  - It will default to calling the network rule 'GL_RULE' which is for gitlab.com and staging.gitlab.com.

3. If you are developing locally and want to update your application, you have to re-run the initial commands and the sql commands,
there is a helper command to both: `make update_local_push`
4. Launch the application via the Snowflake UI and follow the instructions.
   - A short walkthrough can be found on [YouTube](https://www.youtube.com/watch?v=iWXvQpjJ5d4)

## Testing your local changes

1. After you have setup your Snowflake environment, and have successfully run `make complex_create_app_instance_from_app_version`
2. You can access the package by launching the `Data Products > Apps` menu and then launching the deployed App.(E.g., `GITLAB_CONNECTOR_{USERNAME}_INSTANCE`)
3. Continue through the configuration and deploy your instance.
    - If there there is a status code error: `500` after setting the instance and pressing `Connect`, try again.
    - If the error is `401` ensure the token is appropriate for the instance and holds the correct permissions.
    - If after entering a Group name, it asks you to enter a group name again, press Finalize configuration again.

**Field references**:
- Application privileges: Manually grants permissions for application package instance
- Warehouse reference: Select the warehouse from your `~/.snowsql/config`
- Destination database, Database schema: Input names for your database and schema, e.g., `GITLAB_INGESTED_DATABASE_{USERNAME}` and `GITLAB_INGESTED_SCHEMA_{USERNAME}`
- External access integration: Integration name from the setup sql: `GITLAB_INTEGRATION_{USERNAME}`
- Secret: Fully qualified path to secret. E.g., `GITLAB_SECRETS.PUBLIC.GITLAB_PERSONAL_ACCESS_TOKEN_{USERNAME}`
  - Note: Ensure this value is generated from the instance you intend to test against (gitlab.com or staging.gitlab.com, for example)
  - You can generate and store two separate secrets or continue updating your 'default' one.
- GitLab Instance Domain: gitlab.com or staging.gitlab.com; based on Network Rule set, these are the default allowed domains.
- Group name: GitLab group name to ingest data from (e.g., `gitlab-org`)

# Debugging

Debugging the running Native Application isn't easy and comfortable. You can enter the debug mode in order to have
access to all database (application) objects like tables, views etc. that have restricted access. This can be done by
running `ALTER APPLICATION GITLAB_CONNECTOR_USERNAME_INSTANCE SET DEBUG_MODE = TRUE;` in `snowsql`.

## Logging

There are multiple ways in which you can add logs for debugging.

### Using logger and collecting logs to the Event Table

1. We've setup Event table `EVENTS_DATABASE.EVENTS_SCHEMA.EVENTS`.
2. We can push logs to this table from the Java or SQL code as shown in the example below.

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GitLabWorker {
  Logger logger = LoggerFactory.getLogger(GitLabWorker.class);
  try {
    // Do something that can raise an exception
    logger.info("worker done successfully with workerId = " + workerId);    
    return "Executed";
  } catch (Exception exception) {
    logger.error("Exception occurred " + exception.getMessage() + " stacktrace: " + Arrays.toString(exception.getStackTrace()));
    throw new RuntimeException(exception.getMessage());
   }
}
```

For SQL we can use `SYSTEM$LOG` function.
```sql
SYSTEM$LOG_ERROR(TO_VARCHAR(OBJECT_CONSTRUCT('sqlcode', sqlcode, 'sqlerrm', sqlerrm, 'sqlstate', sqlstate)));
```

3. These logs can be queried from the Snowflake UI or using `snowsql`.

```sql
SELECT * FROM EVENTS_DATABASE.EVENTS_SCHEMA.EVENTS WHERE record:severity_text = 'ERROR' ORDER BY TIMESTAMP DESC limit 100;
```

4. For more details read the following:
- [Setup Event Table](https://docs.snowflake.com/en/developer-guide/logging-tracing/event-table-setting-up)
- [Logging in Java](https://docs.snowflake.com/en/developer-guide/logging-tracing/logging-java)
- [Logging in SQL](https://docs.snowflake.com/en/developer-guide/logging-tracing/logging-snowflake-scripting)
- [Event Table columns](https://docs.snowflake.com/en/developer-guide/logging-tracing/event-table-columns)
 
### Creating a draft table and inserting the debug logs to this table.
     
1. In `setup.sql` create a table. Eg: `CREATE TABLE PUBLIC.TEST_TABLE (LOG VARCHAR);` 
2. In the Java files insert into this table. Eg:

```java
session.sql(String.format("INSERT INTO PUBLIC.TEST_TABLE VALUES ('%s')", e.getCause())).collect();
session.sql(String.format("INSERT INTO PUBLIC.TEST_TABLE VALUES ('%s')", e.getMessage())).collect();
session.sql(String.format("INSERT INTO PUBLIC.TEST_TABLE VALUES ('%s')", Arrays.toString(e.getStackTrace()))).collect();
```
### Pushing logs to an external endpoint.

1. In the `GitLabApiHttpClient` class, create a method that allows making HTTP POST requests. For example:

```java
public HttpResponse<String> postLogs(String apiPath, String body) {
  var request = HttpRequest.newBuilder()
    .uri(URI.create(apiPath))
    .POST(HttpRequest.BodyPublishers.ofString(body))
    .header("Content-Type", "application/json")
    .timeout(REQUEST_TIMEOUT)
    .build();

  try {
    return client.send(request, HttpResponse.BodyHandlers.ofString());
  } catch (IOException | InterruptedException ex) {
    throw new RuntimeException(format("HttpRequest failed: %s", ex.getMessage()), ex);
  }
}
```

2. In the `GitlabHttpHelper` class, create a method that POSTs logs to an external API. For example:

```java
 public void logger(String logs) {
   gitlabClient.postLogs("https://snowflake-connector.requestcatcher.com/log", logs);
 }
```

3. Use the above method in the Java files.

```java
new GitlabHttpHelper(this.session).logger("Exception occurred: " + e.getMessage() + " stacktrace: " + Arrays.toString(e.getStackTrace()));
```
4. Ensure that the external URL used for logging is also added to the NETWORK RULE created above.

# Troubleshooting

### Error message: command not found: snowsql

You need to add snowsql to the `Path`, you can run command `export PATH="/Applications/SnowSQL.app/Contents/MacOS:$PATH"` to fix this issue.

### pip install snowflake-snowpark-python failing

If due to some reason your pip install is failing then you can also try `brew install snowflake-snowpark-python`.

# Deploying

1. Ensure that you are on latest master and have no changes locally.
2. Make sure that the PATCH or VERSION in the Makefile is updated from the current released version.
3. You can run the `SHOW RELEASE DIRECTIVES IN APPLICATION PACKAGE GITLAB_CONNECTOR;` command in `snowsql` to check the current release.
4. Create the following diff locally so that the application package name becomes `GITLAB_CONNECTOR` when the release happens
   ```diff
   diff --git a/Makefile b/Makefile
   index 144b205..be0c764 100644
   --- a/Makefile
   +++ b/Makefile
   @@ -1,6 +1,5 @@
   CONNECTION=marketplace_connector_app
   -USERNAME=$(shell whoami)
   -APP_PACKAGE_NAME=GITLAB_CONNECTOR_$(USERNAME)
   +APP_PACKAGE_NAME=GITLAB_CONNECTOR
   INSTANCE_NAME=$(APP_PACKAGE_NAME)_INSTANCE
   SCHEMA_NAME=TEST_SCHEMA
   STAGE_NAME=TEST_STAGE
   ```
5. To release the new version run the command `make release_application` or to release new patch run
`make release_patch`. This will create a new application from the current codebase and will initiate the
[automated security scan](https://docs.snowflake.com/en/developer-guide/native-apps/running-security-scan#about-the-automated-security-scan)
for our application by Snowflake.
6. To check the status of the review run `SHOW VERSIONS IN APPLICATION PACKAGE GITLAB_CONNECTOR;` in `snowsql`.
7. When the review status of the app becomes "approved" run `make create_new_release`. This will update the default
release directive to the latest version and patch of the app.
