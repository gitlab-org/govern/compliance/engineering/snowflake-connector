import streamlit as st
import wizard.wizard_step as ws
from utils.ui_utils import show_vertical_space, show_error, display_gitlab_logo
from native_sdk_api.finalize_config import finalize_connector_configuration


def finalize_config_page():
    display_gitlab_logo()
    st.divider()

    st.header("Finalize connector configuration")
    st.caption(
        "In order to start the connector, you need to finalize the connector configuration by "
        "validating the source. The validation is about requesting the GitLab API endpoint "
        "**https://<your-gitlab-domain>/api/v4/groups/:id/audit_events** to check, if connector will be able to fetch "
        "required data."
    )
    st.divider()

    st.subheader("Group name")
    input_col, _ = st.columns([2, 1])
    with input_col:
        group_name_st = st.text_input("", key="group_name_tb", label_visibility="collapsed")
        st.caption("Name of the Group for which the audit events will be fetched")
    if st.session_state.get("is_group_tb_empty"):
        st.error("Group name text box can not be empty. Please fill it.")
    st.divider()

    if st.session_state.get("show_main_error"):
        st.error(st.session_state.get("error_msg"))
    _, finalize_btn_col = st.columns([2.95, 1.05])
    with finalize_btn_col:
        st.button(
            "Finalize configuration",
            on_click=finalize_configuration,
            args=(group_name_st,),
            type="primary"
        )


def are_text_boxes_empty(group_name_tb: str):
    result = False
    if not group_name_tb:
        st.session_state["is_group_tb_empty"] = True
        result = True
    else:
        st.session_state["is_group_tb_empty"] = False
    return result


def finalize_configuration(group_name: str):
    if are_text_boxes_empty(group_name):
        return

    try:
        st.session_state["show_main_error"] = False
        response = finalize_connector_configuration(group_name)
        if response.is_ok():
            ws.change_step(ws.FINALIZE_CONFIG)
        else:
            st.session_state["error_msg"] = f"An error response with code {response.get_response_code()} " \
                                            f"has been returned by FINALIZE_CONNECTOR_CONFIGURATION: " \
                                            f"{response.get_message()}"
            st.session_state["show_main_error"] = True
    except:
        show_error("Unexpected error occurred, correct the provided data and try again")
