import streamlit as st
from native_sdk_api.connector_response import ConnectorResponse
import PIL.Image as image
import numpy as np
import plotly.express as px


def show_vertical_space(empty_lines: int):
    for _ in range(empty_lines):
        st.write("")


def show_error(error: str):
    st.error(error)


def show_error_response(response: ConnectorResponse, called_procedure: str):
    st.error(
        f"An error response with code {response.get_response_code()} has been returned by "
        f"{called_procedure}: {response.get_message()}"
    )


def display_gitlab_logo(small=False):
    img = 'gitlab-logo.jpg'
    im_np = np.asarray(image.open(img))
    fig = px.imshow(im_np)
    fig.update_traces(hovertemplate=None, hoverinfo="skip")
    if small:
        fig.update_layout(width=700, height=100, margin=dict(l=0, r=0, b=0, t=0))
    else:
        fig.update_layout(width=700, height=200, margin=dict(l=0, r=0, b=0, t=0))
    fig.update_xaxes(showticklabels=False, showgrid=False, zeroline=False)
    fig.update_yaxes(showticklabels=False, showgrid=False, zeroline=False)
    fig.layout.xaxis.fixedrange = True
    fig.layout.yaxis.fixedrange = True
    st.plotly_chart(fig, config={'displayModeBar': False})
