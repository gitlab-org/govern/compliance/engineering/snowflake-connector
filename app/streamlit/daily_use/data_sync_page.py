import streamlit as st
from daily_use.sync_status_bar import sync_status_bar
from native_sdk_api.resource_management import (
    create_resource_for_group,
    create_resource_for_project,
    fetch_instance_resources,
    fetch_group_resources,
    fetch_project_resources
)


def queue_group_resource():
    group_name = st.session_state.get("group_name")

    if not group_name:
        st.error("Group name cannot be empty.")
        return

    result = create_resource_for_group(group_name)
    if result.is_ok():
        st.success("Resource created")
    else:
        st.error(result.get_message())


def queue_project_resource():
    project_name = st.session_state.get("project_name")

    if not project_name:
        st.error("Project name cannot be empty.")
        return

    result = create_resource_for_project(project_name)
    if result.is_ok():
        st.success("Resource created")
    else:
        st.error(result.get_message())


def data_sync_page():
    sync_status_bar()
    groups_tab, projects_tab = st.tabs(["Enabled Groups", "Enabled Projects"])

    with groups_tab:
        groups_page()
    with projects_tab:
        projects_page()


def groups_page():
    st.subheader("Enabled Groups")
    with st.form("add_new_group_resource_form", clear_on_submit=True):
        st.caption("Enable ingestion for new groups")
        st.text_input(
            "Group name",
            key="group_name",
        )
        _ = st.form_submit_button(
            "Queue ingestion",
            on_click=queue_group_resource
        )

    group_resources = fetch_group_resources().to_pandas()
    st.table(group_resources)


def projects_page():
    st.subheader("Enabled Projects")
    with st.form("add_new_project_resource_form", clear_on_submit=True):
        st.caption("Enable ingestion for new projects. Enter full path, Eg: gitlab-org/gitlab")
        st.text_input(
            "Project name",
            key="project_name",
        )
        _ = st.form_submit_button(
            "Queue ingestion",
            on_click=queue_project_resource
        )

    project_resources = fetch_project_resources().to_pandas()
    st.table(project_resources)
