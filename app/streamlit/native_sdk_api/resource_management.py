from utils.sql_utils import call_procedure, varchar_argument, variant_argument, variant_list_argument
from snowflake.snowpark.context import get_active_session

session = get_active_session()

INSTANCE_TYPE = "instance"
GROUP_TYPE = "group"
PROJECT_TYPE = "project"


def create_resource(type, resource_id_params):
    if type != INSTANCE_TYPE and type != GROUP_TYPE and type != PROJECT_TYPE:
        raise Exception(f"{type} is not valid. Type should either be 'instance', 'group' or 'project'")

    ingestion_config = [{
        "id": "ingestionConfig",
        "ingestionStrategy": "INCREMENTAL",
        "scheduleType": "INTERVAL",
        "scheduleDefinition": "60m"
    }]

    resource_id = {"type": type}
    resource_id.update(resource_id_params)

    if type == INSTANCE_TYPE:
        id = INSTANCE_TYPE
    elif type == GROUP_TYPE:
        id = f"{GROUP_TYPE}.{resource_id_params['group_name']}"
    else:
        id = f"{PROJECT_TYPE}.{resource_id_params['project_name']}"

    return call_procedure("PUBLIC.CREATE_RESOURCE",
                          [
                              varchar_argument(id),
                              variant_argument(resource_id),
                              variant_list_argument(ingestion_config),
                              varchar_argument(id),
                              "true"
                          ])


def create_resource_for_instance():
    return create_resource(INSTANCE_TYPE, {})


def create_resource_for_group(group_name):
    return create_resource(GROUP_TYPE, {"group_name": group_name})


def create_resource_for_project(project_name):
    return create_resource(PROJECT_TYPE, {"project_name": project_name})


def fetch_instance_resources():
    return session.sql(
        """
       SELECT
         IS_ENABLED
       FROM PUBLIC.INGESTION_DEFINITIONS
       WHERE resource_id:type='instance'
       """
    )


def fetch_group_resources():
    return session.sql(
        """
       SELECT
         resource_id:group_name::string AS group_name,
         IS_ENABLED
       FROM PUBLIC.INGESTION_DEFINITIONS
       WHERE resource_id:type='group'
       """
    )


def fetch_project_resources():
    return session.sql(
        """
       SELECT
         resource_id:project_name::string AS project_name,
         IS_ENABLED
       FROM PUBLIC.INGESTION_DEFINITIONS
       WHERE resource_id:type='project'
       """
    )
