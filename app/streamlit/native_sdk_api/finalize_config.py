from utils.sql_utils import call_procedure, variant_argument
from native_sdk_api.resource_management import (
    create_resource_for_instance,
    create_resource_for_group
)


def finalize_connector_configuration(group_name: str):
    config = {"group_name": group_name}
    response = call_procedure("PUBLIC.FINALIZE_CONNECTOR_CONFIGURATION", [variant_argument(config)])
    if response.is_ok():
        response = create_resource_for_instance()
    if response.is_ok():
        response = create_resource_for_group(group_name)
    return response
