EXECUTE IMMEDIATE $$
DECLARE STATUSES VARIANT DEFAULT (
        SELECT value
        FROM STATE.APP_STATE
        WHERE KEY = 'connector_status'
    );
CONNECTOR_CONFIGURATION_STATUS VARCHAR DEFAULT (STATUSES :configurationStatus);
DESTINATION_DATABASE STRING DEFAULT (
    SELECT value :destination_database
    FROM STATE.APP_CONFIG
    where key = 'connector_configuration'
);
DESTINATION_SCHEMA STRING DEFAULT (
    SELECT value :destination_schema
    FROM STATE.APP_CONFIG
    where key = 'connector_configuration'
);
FULLY_QUALIFIED_PROJECTS_TABLE VARCHAR DEFAULT (
    SELECT CONCAT_WS(
            '.',
            :DESTINATION_DATABASE,
            :DESTINATION_SCHEMA,
            'PROJECTS'
        )
);
FULLY_QUALIFIED_PROJECTS_VIEW VARCHAR DEFAULT (
    SELECT CONCAT_WS(
            '.',
            :DESTINATION_DATABASE,
            :DESTINATION_SCHEMA,
            'PROJECTS_VIEW'
        )
);
BEGIN IF(:CONNECTOR_CONFIGURATION_STATUS = 'FINALIZED') THEN CREATE TABLE IF NOT EXISTS identifier(:FULLY_QUALIFIED_PROJECTS_TABLE) (RAW_DATA VARIANT);
GRANT ALL ON TABLE identifier(:FULLY_QUALIFIED_PROJECTS_TABLE) TO APPLICATION ROLE ADMIN;
GRANT SELECT ON TABLE identifier(:FULLY_QUALIFIED_PROJECTS_TABLE) TO APPLICATION ROLE DATA_READER;
CREATE VIEW IF NOT EXISTS identifier(:FULLY_QUALIFIED_PROJECTS_VIEW) AS (
    SELECT RAW_DATA ['id'] as id,
        RAW_DATA ['description'] as description,
        RAW_DATA ['name'] as name,
        RAW_DATA ['name_with_namespace'] as name_with_namespace,
        RAW_DATA ['path'] as path,
        RAW_DATA ['path_with_namespace'] as path_with_namespace,
        RAW_DATA ['created_at'] as created_at,
        RAW_DATA ['default_branch'] as default_branch,
        RAW_DATA ['tag_list'] as tag_list,
        RAW_DATA ['topics'] as topics,
        RAW_DATA ['ssh_url_to_repo'] as ssh_url_to_repo,
        RAW_DATA ['http_url_to_repo'] as http_url_to_repo,
        RAW_DATA ['web_url'] as web_url,
        RAW_DATA ['readme_url'] as readme_url,
        RAW_DATA ['forks_count'] as forks_count,
        RAW_DATA ['avatar_url'] as avatar_url,
        RAW_DATA ['star_count'] as star_count,
        RAW_DATA ['last_activity_at'] as last_activity_at,
        RAW_DATA ['namespace'] as namespace,
        RAW_DATA ['container_registry_image_prefix'] as container_registry_image_prefix,
        RAW_DATA ['_links'] as _links,
        RAW_DATA ['packages_enabled'] as packages_enabled,
        RAW_DATA ['empty_repo'] as empty_repo,
        RAW_DATA ['archived'] as archived,
        RAW_DATA ['visibility'] as visibility,
        RAW_DATA ['owner'] as owner,
        RAW_DATA ['resolve_outdated_diff_discussions'] as resolve_outdated_diff_discussions,
        RAW_DATA ['container_expiration_policy'] as container_expiration_policy,
        RAW_DATA ['repository_object_format'] as repository_object_format,
        RAW_DATA ['issues_enabled'] as issues_enabled,
        RAW_DATA ['merge_requests_enabled'] as merge_requests_enabled,
        RAW_DATA ['wiki_enabled'] as wiki_enabled,
        RAW_DATA ['jobs_enabled'] as jobs_enabled,
        RAW_DATA ['snippets_enabled'] as snippets_enabled,
        RAW_DATA ['container_registry_enabled'] as container_registry_enabled,
        RAW_DATA ['service_desk_enabled'] as service_desk_enabled,
        RAW_DATA ['can_create_merge_request_in'] as can_create_merge_request_in,
        RAW_DATA ['issues_access_level'] as issues_access_level,
        RAW_DATA ['repository_access_level'] as repository_access_level,
        RAW_DATA ['merge_requests_access_level'] as merge_requests_access_level,
        RAW_DATA ['forking_access_level'] as forking_access_level,
        RAW_DATA ['wiki_access_level'] as wiki_access_level,
        RAW_DATA ['builds_access_level'] as builds_access_level,
        RAW_DATA ['snippets_access_level'] as snippets_access_level,
        RAW_DATA ['pages_access_level'] as pages_access_level,
        RAW_DATA ['analytics_access_level'] as analytics_access_level,
        RAW_DATA ['container_registry_access_level'] as container_registry_access_level,
        RAW_DATA ['security_and_compliance_access_level'] as security_and_compliance_access_level,
        RAW_DATA ['releases_access_level'] as releases_access_level,
        RAW_DATA ['environments_access_level'] as environments_access_level,
        RAW_DATA ['feature_flags_access_level'] as feature_flags_access_level,
        RAW_DATA ['infrastructure_access_level'] as infrastructure_access_level,
        RAW_DATA ['monitor_access_level'] as monitor_access_level,
        RAW_DATA ['model_experiments_access_level'] as model_experiments_access_level,
        RAW_DATA ['model_registry_access_level'] as model_registry_access_level,
        RAW_DATA ['emails_disabled'] as emails_disabled,
        RAW_DATA ['emails_enabled'] as emails_enabled,
        RAW_DATA ['shared_runners_enabled'] as shared_runners_enabled,
        RAW_DATA ['lfs_enabled'] as lfs_enabled,
        RAW_DATA ['creator_id'] as creator_id,
        RAW_DATA ['import_status'] as import_status,
        RAW_DATA ['open_issues_count'] as open_issues_count,
        RAW_DATA ['description_html'] as description_html,
        RAW_DATA ['updated_at'] as updated_at,
        RAW_DATA ['ci_config_path'] as ci_config_path,
        RAW_DATA ['public_jobs'] as public_jobs,
        RAW_DATA ['shared_with_groups'] as shared_with_groups,
        RAW_DATA ['only_allow_merge_if_pipeline_succeeds'] as only_allow_merge_if_pipeline_succeeds,
        RAW_DATA ['allow_merge_on_skipped_pipeline'] as allow_merge_on_skipped_pipeline,
        RAW_DATA ['request_access_enabled'] as request_access_enabled,
        RAW_DATA ['only_allow_merge_if_all_discussions_are_resolved'] as only_allow_merge_if_all_discussions_are_resolved,
        RAW_DATA ['remove_source_branch_after_merge'] as remove_source_branch_after_merge,
        RAW_DATA ['printing_merge_request_link_enabled'] as printing_merge_request_link_enabled,
        RAW_DATA ['merge_method'] as merge_method,
        RAW_DATA ['squash_option'] as squash_option,
        RAW_DATA ['enforce_auth_checks_on_uploads'] as enforce_auth_checks_on_uploads,
        RAW_DATA ['suggestion_commit_message'] as suggestion_commit_message,
        RAW_DATA ['merge_commit_template'] as merge_commit_template,
        RAW_DATA ['squash_commit_template'] as squash_commit_template,
        RAW_DATA ['issue_branch_template'] as issue_branch_template,
        RAW_DATA ['warn_about_potentially_unwanted_characters'] as warn_about_potentially_unwanted_characters,
        RAW_DATA ['autoclose_referenced_issues'] as autoclose_referenced_issues,
        RAW_DATA ['external_authorization_classification_label'] as external_authorization_classification_label,
        RAW_DATA ['requirements_enabled'] as requirements_enabled,
        RAW_DATA ['requirements_access_level'] as requirements_access_level,
        RAW_DATA ['security_and_compliance_enabled'] as security_and_compliance_enabled,
        RAW_DATA ['compliance_frameworks'] as compliance_frameworks,
        RAW_DATA ['permissions'] as permissions,
        from identifier(:FULLY_QUALIFIED_PROJECTS_TABLE)
);
GRANT SELECT ON VIEW identifier(:FULLY_QUALIFIED_PROJECTS_VIEW) TO APPLICATION ROLE ADMIN;
GRANT SELECT ON VIEW identifier(:FULLY_QUALIFIED_PROJECTS_VIEW) TO APPLICATION ROLE DATA_READER;
END IF;
END;
$$;
