EXECUTE IMMEDIATE
$$
DECLARE
    STATUSES VARIANT DEFAULT (SELECT value FROM STATE.APP_STATE WHERE KEY = 'connector_status');
    CONNECTOR_CONFIGURATION_STATUS VARCHAR DEFAULT (STATUSES:configurationStatus);
    DESTINATION_DATABASE STRING DEFAULT (SELECT value:destination_database FROM STATE.APP_CONFIG where key = 'connector_configuration');
    DESTINATION_SCHEMA STRING DEFAULT (SELECT value:destination_schema FROM STATE.APP_CONFIG where key = 'connector_configuration');
    FULLY_QUALIFIED_USERS_TABLE VARCHAR DEFAULT (SELECT CONCAT_WS('.', :DESTINATION_DATABASE, :DESTINATION_SCHEMA, 'USERS'));
    FULLY_QUALIFIED_USERS_VIEW VARCHAR DEFAULT (SELECT CONCAT_WS('.', :DESTINATION_DATABASE, :DESTINATION_SCHEMA, 'USERS_VIEW'));
BEGIN
    IF(:CONNECTOR_CONFIGURATION_STATUS = 'FINALIZED') THEN
        CREATE TABLE IF NOT EXISTS identifier(:FULLY_QUALIFIED_USERS_TABLE) (RAW_DATA VARIANT);
        GRANT ALL ON TABLE identifier(:FULLY_QUALIFIED_USERS_TABLE) TO APPLICATION ROLE ADMIN;
        GRANT SELECT ON TABLE identifier(:FULLY_QUALIFIED_USERS_TABLE) TO APPLICATION ROLE DATA_READER;

        CREATE VIEW IF NOT EXISTS identifier(:FULLY_QUALIFIED_USERS_VIEW) AS (SELECT RAW_DATA['id'] as id,
            RAW_DATA['username'] as username,
            RAW_DATA['name'] as name,
            RAW_DATA['state'] as state,
            RAW_DATA['avatar_url'] as avatar_url,
            RAW_DATA['web_url'] as web_url,
            RAW_DATA['created_at'] as created_at,
            RAW_DATA['bio'] as bio,
            RAW_DATA['location'] as location,
            RAW_DATA['public_email'] as public_email,
            RAW_DATA['skype'] as skype,
            RAW_DATA['linkedin'] as linkedin,
            RAW_DATA['twitter'] as twitter,
            RAW_DATA['discord'] as discord,
            RAW_DATA['website_url'] as website_url,
            RAW_DATA['organization'] as organization,
            RAW_DATA['job_title'] as job_title,
            RAW_DATA['pronouns'] as pronouns,
            RAW_DATA['bot'] as bot,
            RAW_DATA['work_information'] as work_information,
            RAW_DATA['followers'] as total_followers,
            RAW_DATA['following'] as total_following,
            RAW_DATA['is_followed'] as is_followed,
            RAW_DATA['local_time'] as local_time,
            RAW_DATA['last_sign_in_at'] as last_sign_in_at,
            RAW_DATA['email'] as email,
            RAW_DATA['theme_id'] as theme_id,
            RAW_DATA['color_scheme_id'] as color_scheme_id,
            RAW_DATA['projects_limit'] as projects_limit,
            RAW_DATA['current_sign_in_at'] as current_sign_in_at,
            RAW_DATA['identities'] as identities,
            RAW_DATA['can_create_group'] as can_create_group,
            RAW_DATA['can_create_project'] as can_create_project,
            RAW_DATA['two_factor_enabled'] as two_factor_enabled,
            RAW_DATA['external'] as external,
            RAW_DATA['private_profile'] as private_profile,
            RAW_DATA['commit_email'] as commit_email,
            RAW_DATA['shared_runners_minutes_limit'] as shared_runners_minutes_limit,
            RAW_DATA['extra_shared_runners_minutes_limit'] as extra_shared_runners_minutes_limit,
            RAW_DATA['scim_identities'] as scim_identities,
            RAW_DATA['is_admin'] as is_admin,
            RAW_DATA['note'] as note,
            RAW_DATA['namespace_id'] as namespace_id,
            RAW_DATA['created_by']['id'] as created_by_id,
            RAW_DATA['created_by']['username'] as created_by_username,
            RAW_DATA['created_by']['name'] as created_by_name,
            RAW_DATA['created_by']['avatar_url'] as created_by_avatar_url,
            RAW_DATA['created_by']['web_url'] as created_by_web_url,
            RAW_DATA['email_reset_offered_at'] as email_reset_offered_at,
            RAW_DATA['using_license_seat'] as using_license_seat,
            RAW_DATA['is_auditor'] as is_auditor,
            RAW_DATA['provisioned_by_group_id'] as provisioned_by_group_id,
            RAW_DATA['enterprise_group_id'] as enterprise_group_id,
            RAW_DATA['enterprise_group_associated_at'] as enterprise_group_associated_at,
            from identifier(:FULLY_QUALIFIED_USERS_TABLE));
        GRANT SELECT ON VIEW identifier(:FULLY_QUALIFIED_USERS_VIEW) TO APPLICATION ROLE ADMIN;
        GRANT SELECT ON VIEW identifier(:FULLY_QUALIFIED_USERS_VIEW) TO APPLICATION ROLE DATA_READER;
    END IF;
END;
$$
;
