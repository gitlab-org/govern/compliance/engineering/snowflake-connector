EXECUTE IMMEDIATE
$$
DECLARE
    STATUSES VARIANT DEFAULT (SELECT value FROM STATE.APP_STATE WHERE KEY = 'connector_status');
    CONNECTOR_CONFIGURATION_STATUS VARCHAR DEFAULT (STATUSES:configurationStatus);
    GITLAB_PAT_SECRET_NAME VARCHAR DEFAULT (SELECT VALUE FROM PUBLIC.CONNECTOR_CONFIGURATION WHERE CONFIG_GROUP='connection_configuration' AND CONFIG_KEY='secret');
    EXTERNAL_ACCESS_INTEGRATION_NAME VARCHAR DEFAULT (SELECT VALUE FROM PUBLIC.CONNECTOR_CONFIGURATION WHERE CONFIG_GROUP='connection_configuration' AND CONFIG_KEY='external_access_integration');
BEGIN
    IF(:CONNECTOR_CONFIGURATION_STATUS IN ('CONNECTED', 'FINALIZED')) THEN
        CALL PUBLIC.CONFIGURE_PROCEDURES(:GITLAB_PAT_SECRET_NAME, :EXTERNAL_ACCESS_INTEGRATION_NAME);
    END IF;
EXCEPTION
    WHEN statement_error THEN
        SYSTEM$LOG_ERROR(TO_VARCHAR(OBJECT_CONSTRUCT('sqlcode', sqlcode, 'sqlerrm', sqlerrm, 'sqlstate', sqlstate)));

        IF (CONTAINS(sqlerrm, 'Insufficient privileges to operate on integration') OR CONTAINS(sqlerrm, 'does not exist or operation not authorized')) THEN
            -- If required permissions not granted then catch the exception so that the upgrade proceeds successfully.
            RETURN OBJECT_CONSTRUCT('response_code', 'OK');
        END IF;
        RAISE; -- Raise the same exception.
END;
$$
;
