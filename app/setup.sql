-- CONNECTORS-NATIVE-SDK
EXECUTE IMMEDIATE FROM 'native-connectors-sdk-components/all.sql';
EXECUTE IMMEDIATE FROM 'native-connectors-sdk-components/task_reactor.sql';

-- CUSTOM CONNECTOR OBJECTS
CREATE OR ALTER VERSIONED SCHEMA STREAMLIT;
GRANT USAGE ON SCHEMA STREAMLIT TO APPLICATION ROLE ADMIN;

CREATE OR REPLACE STREAMLIT STREAMLIT.GITLAB_CONNECTOR_ST
    FROM  '/streamlit'
    MAIN_FILE = 'streamlit_app.py';
GRANT USAGE ON STREAMLIT STREAMLIT.GITLAB_CONNECTOR_ST TO APPLICATION ROLE ADMIN;

-- SNOWFLAKE REFERENCE MECHANISM
CREATE OR REPLACE PROCEDURE PUBLIC.REGISTER_REFERENCE(ref_name STRING, operation STRING, ref_or_alias STRING)
    RETURNS STRING
    LANGUAGE SQL
    AS
        BEGIN
            CASE (operation)
                WHEN 'ADD' THEN
                    SELECT SYSTEM$SET_REFERENCE(:ref_name, :ref_or_alias);
                WHEN 'REMOVE' THEN
                    SELECT SYSTEM$REMOVE_REFERENCE(:ref_name);
                WHEN 'CLEAR' THEN
                    SELECT SYSTEM$REMOVE_REFERENCE(:ref_name);
                ELSE RETURN 'unknown operation: ' || operation;
                END CASE;
            RETURN NULL;
        END;
GRANT USAGE ON PROCEDURE PUBLIC.REGISTER_REFERENCE(STRING, STRING, STRING) TO APPLICATION ROLE ADMIN;

-----------------WIZARD-----------------
-- PREREQUISITES
MERGE INTO STATE.PREREQUISITES AS dest
    USING (SELECT * FROM VALUES
               ('1',
                'GitLab account',
                'Prepare a GitLab account, from which particular resources will be fetched.',
                'https://docs.gitlab.com/ee/user/profile/account/create_accounts.html',
                1
               ),
               ('2',
                'GitLab personal access token',
                'Prepare a personal GitLab access token in order to give the connector an access to the account.',
                'https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token',
                2
               ),
               ('3',
                'External Access Integration',
                'It is required to create an External Access Integration that allows to connect with the GitLab API. To do that, you need to create a Secret with the GitLab personal access token and a Network Rule that allows to connect to your GitLab instance, https://gitlab.example.com.',
                'https://docs.snowflake.com/en/developer-guide/external-network-access/creating-using-external-network-access',
                3
               )
    ) AS src (id, title, description, documentation_url, position)
    ON dest.id = src.id
    WHEN NOT MATCHED THEN
        INSERT (id, title, description, documentation_url, position)
        VALUES (src.id, src.title, src.description, src.documentation_url, src.position);

-- CONNECTION CONFIGURATION
CREATE OR REPLACE PROCEDURE PUBLIC.SET_CONNECTION_CONFIGURATION(connection_configuration VARIANT)
    RETURNS VARIANT
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.configuration.connection.GitlabConnectionConfigurationHandler.setConnectionConfiguration';

CREATE OR REPLACE PROCEDURE PUBLIC.TEST_CONNECTION()
    RETURNS VARIANT
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.configuration.connection.GitlabConnectionValidator.testConnection';

-- FINALIZE CONFIGURATION
CREATE OR REPLACE PROCEDURE PUBLIC.FINALIZE_CONNECTOR_CONFIGURATION(CUSTOM_CONFIGURATION VARIANT)
    RETURNS VARIANT
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.configuration.finalize.FinalizeConnectorConfigurationCustomHandler.finalizeConnectorConfiguration';

-----------------TASK REACTOR-----------------
CREATE OR REPLACE PROCEDURE PUBLIC.GITLAB_WORKER(worker_id number, task_reactor_schema string)
    RETURNS STRING
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0', 'com.snowflake:telemetry:latest')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.ingestion.GitLabWorker.executeWork';

CALL TASK_REACTOR.CREATE_INSTANCE_OBJECTS(
        'GITLAB_CONNECTOR_TASK_REACTOR',
        'PUBLIC.GITLAB_WORKER',
        'VIEW',
        'GITLAB_CONNECTOR_TASK_REACTOR.WORK_SELECTOR_VIEW',
        NULL
     );

CREATE OR REPLACE VIEW GITLAB_CONNECTOR_TASK_REACTOR.WORK_SELECTOR_VIEW AS SELECT * FROM GITLAB_CONNECTOR_TASK_REACTOR.QUEUE ORDER BY RESOURCE_ID;

CREATE OR REPLACE PROCEDURE PUBLIC.RUN_SCHEDULER_ITERATION()
    RETURNS VARIANT
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.integration.SchedulerIntegratedWithTaskReactorHandler.runIteration';

-----------------LIFECYCLE-----------------
CREATE OR REPLACE PROCEDURE PUBLIC.PAUSE_CONNECTOR()
    RETURNS VARIANT
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.lifecycle.pause.PauseConnectorCustomHandler.pauseConnector';
GRANT USAGE ON PROCEDURE PUBLIC.PAUSE_CONNECTOR() TO APPLICATION ROLE ADMIN;

CREATE OR REPLACE PROCEDURE PUBLIC.RESUME_CONNECTOR()
    RETURNS VARIANT
    LANGUAGE JAVA
    RUNTIME_VERSION = '11'
    PACKAGES = ('com.snowflake:snowpark:1.11.0')
    IMPORTS = ('/snowflake-connector.jar', '/connectors-native-sdk.jar')
    HANDLER = 'com.gitlab.connectors.lifecycle.resume.ResumeConnectorCustomHandler.resumeConnector';
GRANT USAGE ON PROCEDURE PUBLIC.RESUME_CONNECTOR() TO APPLICATION ROLE ADMIN;

-------------------CONFIGURE PROCEDURES-------------------
CREATE OR REPLACE PROCEDURE PUBLIC.CONFIGURE_PROCEDURES(secret_path STRING, external_access_integration STRING)
    RETURNS VARIANT
    LANGUAGE SQL
    AS
        BEGIN
            LET ALTER_TEST_CONNECTION_SQL VARCHAR := 'ALTER PROCEDURE PUBLIC.TEST_CONNECTION() SET SECRETS=(\'gitlab_personal_access_token\'=' || :secret_path || ') EXTERNAL_ACCESS_INTEGRATIONS=(' || :external_access_integration || ');';
            LET ALTER_GITLAB_WORKER_SQL VARCHAR := 'ALTER PROCEDURE PUBLIC.GITLAB_WORKER(NUMBER, STRING) SET SECRETS=(\'gitlab_personal_access_token\'=' || :secret_path || ') EXTERNAL_ACCESS_INTEGRATIONS=(' || :external_access_integration || ');';
            LET ALTER_FINALIZE_CONNECTOR_CONFIGURATION_SQL VARCHAR := 'ALTER PROCEDURE FINALIZE_CONNECTOR_CONFIGURATION(VARIANT) SET SECRETS=(\'gitlab_personal_access_token\'=' || :secret_path || ') EXTERNAL_ACCESS_INTEGRATIONS=(' || :external_access_integration || ');';
            EXECUTE IMMEDIATE :ALTER_TEST_CONNECTION_SQL;
            EXECUTE IMMEDIATE :ALTER_GITLAB_WORKER_SQL;
            EXECUTE IMMEDIATE :ALTER_FINALIZE_CONNECTOR_CONFIGURATION_SQL;
            RETURN OBJECT_CONSTRUCT('response_code', 'OK');
        END;

EXECUTE IMMEDIATE FROM 'trigger_configure_procedures.sql';

-----------------EXECUTE DATABASE MIGRATIONS-----------------
EXECUTE IMMEDIATE FROM 'db/migrations/20240707172555_create_users_table.sql';
EXECUTE IMMEDIATE FROM 'db/migrations/20250106173132_create_projects_table.sql';
