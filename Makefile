CONNECTION=marketplace_connector_app
USERNAME=$(shell whoami)
APP_PACKAGE_NAME=GITLAB_CONNECTOR_$(USERNAME)
INSTANCE_NAME=$(APP_PACKAGE_NAME)_INSTANCE
SCHEMA_NAME=TEST_SCHEMA
STAGE_NAME=TEST_STAGE
VERSION=2.0
PATCH=0
VERSION_DIR_PATH=@$(APP_PACKAGE_NAME).$(SCHEMA_NAME).$(STAGE_NAME)/$(VERSION).$(PATCH)

# ***********
#  CORE FLOW
# ***********

.PHONY: copy_internal_components
copy_internal_components:
	./gradlew copyInternalComponents

.PHONY: copy_sdk_components
copy_sdk_components:
	./gradlew copySdkComponents

.PHONY: prepare_app_package
prepare_app_package:
	./gradlew prepareAppPackage \
		-Pconnection=$(CONNECTION) \
		-PappPackage=$(APP_PACKAGE_NAME) \
		-Pschema=$(SCHEMA_NAME) \
		-Pstage=$(STAGE_NAME)

.PHONY: deploy_connector
deploy_connector:
	./gradlew deployConnector \
		-Pconnection=$(CONNECTION) \
		-PappPackage=$(APP_PACKAGE_NAME) \
		-Pschema=$(SCHEMA_NAME) \
		-Pstage=$(STAGE_NAME) \
		-PappVersion=$(VERSION) \
		-PappPatch=$(PATCH)

# ****************************************
#  CREATE INSTANCE FROM VERSION DIRECTORY
# ****************************************

.PHONY: create_app_instance_from_version_dir
create_app_instance_from_version_dir:
	./gradlew createAppInstance \
		-Pconnection=$(CONNECTION) \
		-PappPackage=$(APP_PACKAGE_NAME) \
		-PversionDirPath=$(VERSION_DIR_PATH)

.PHONY: complex_create_app_instance_from_version_dir
complex_create_app_instance_from_version_dir:
	make copy_internal_components
	make copy_sdk_components
	make prepare_app_package
	make deploy_connector
	make create_app_instance_from_version_dir

# **********************************
#  CREATE INSTANCE FROM APP VERSION
# **********************************

.PHONY: create_new_version
create_new_version:
	./gradlew createNewVersion \
		-Pconnection=$(CONNECTION) \
		-PappPackage=$(APP_PACKAGE_NAME) \
		-PversionDirPath=$(VERSION_DIR_PATH) \
		-PappVersion=$(VERSION)

.PHONY: create_app_instance_from_app_version
create_app_instance_from_app_version:
	./gradlew createAppInstance \
		-Pconnection=$(CONNECTION) \
		-PappPackage=$(APP_PACKAGE_NAME) \
		-PappVersion=$(VERSION)

.PHONY: complex_create_app_instance_from_app_version
complex_create_app_instance_from_app_version:
	make copy_internal_components
	make copy_sdk_components
	make prepare_app_package
	make deploy_connector
	make create_new_version
	make create_app_instance_from_app_version

# ******************
#  ADDITIONAL TASKS
# ******************

.PHONY: drop_application
drop_application:
	snowsql -c $(CONNECTION) \
		-q "DROP APPLICATION IF EXISTS $(INSTANCE_NAME) CASCADE; DROP APPLICATION PACKAGE IF EXISTS $(APP_PACKAGE_NAME)"

.PHONY: reinstall_application_from_version_dir
reinstall_application_from_version_dir:
	make drop_application
	make complex_create_app_instance_from_version_dir

.PHONY: reinstall_application_from_app_version
reinstall_application_from_app_version:
	make drop_application
	make complex_create_app_instance_from_app_version


# **************************
#  CREATE RELEASE DIRECTIVE
# **************************

.PHONY: create_new_release
create_new_release:
	./gradlew createNewRelease \
    		-Pconnection=$(CONNECTION) \
    		-PappPackage=$(APP_PACKAGE_NAME) \
    		-PappVersion=$(VERSION) \
    		-PappPatch=$(PATCH)

.PHONY: set_distribution_to_external
set_distribution_to_external:
	./gradlew setDistributionToExternal \
        		-Pconnection=$(CONNECTION) \
        		-PappPackage=$(APP_PACKAGE_NAME) \

.PHONY: release_application
release_application:
	make copy_internal_components
	make copy_sdk_components
	make deploy_connector
	make create_new_version
	make create_new_release

.PHONY: release_patch
release_patch:
	make copy_internal_components
	make copy_sdk_components
	make deploy_connector
	make create_new_patch

.PHONY: create_new_patch
create_new_patch:
	./gradlew createNewPatch \
		-Pconnection=$(CONNECTION) \
		-PappPackage=$(APP_PACKAGE_NAME) \
		-PversionDirPath=$(VERSION_DIR_PATH) \
		-PappVersion=$(VERSION) \
		-PappPatch=$(PATCH)

# **************************
#  Linting and Formatting
# **************************

.PHONY: refresh_dependencies
refresh_dependencies:
	./gradlew --refresh-dependencies

.PHONY: lint
lint:
	./gradlew spotlessCheck

.PHONY: format
format:
	./gradlew spotlessApply

# **************************
#  LOCAL DEVELOPMENT HELPERS
# **************************

# Runs the SQL script to grant permissions for our integrations, with the default being against gitlab.com or staging.gitlab.com
.PHONY: setup_integration_sql
setup_integration_sql:
	@read -sp "Enter GitLab token: " token && echo && \
	snowsql -c $(CONNECTION) -q " \
		CREATE DATABASE IF NOT EXISTS GITLAB_SECRETS; \
		USE GITLAB_SECRETS; \
		CREATE OR REPLACE SECRET GITLAB_PERSONAL_ACCESS_TOKEN_$(USERNAME) \
			TYPE=GENERIC_STRING SECRET_STRING='$$token'; \
		CREATE OR REPLACE NETWORK RULE GL_RULE \
			MODE = EGRESS TYPE = HOST_PORT \
			VALUE_LIST=('gitlab.com:443', 'staging.gitlab.com:443'); \
		CREATE OR REPLACE EXTERNAL ACCESS INTEGRATION GITLAB_INTEGRATION_$(USERNAME) \
			ALLOWED_NETWORK_RULES = (GL_RULE) \
			ALLOWED_AUTHENTICATION_SECRETS = ('GITLAB_SECRETS.PUBLIC.GITLAB_PERSONAL_ACCESS_TOKEN_$(USERNAME)') \
			ENABLED = TRUE; \
		GRANT USAGE ON INTEGRATION GITLAB_INTEGRATION_$(USERNAME) TO APPLICATION $(INSTANCE_NAME); \
		GRANT USAGE ON DATABASE GITLAB_SECRETS TO APPLICATION $(INSTANCE_NAME); \
		GRANT USAGE ON SCHEMA GITLAB_SECRETS.PUBLIC TO APPLICATION $(INSTANCE_NAME); \
		GRANT READ ON SECRET GITLAB_SECRETS.PUBLIC.GITLAB_PERSONAL_ACCESS_TOKEN_$(USERNAME) TO APPLICATION $(INSTANCE_NAME);"

.PHONY: update_local_push
update_local_push:
	make reinstall_application_from_app_version
	make setup_integration_sql

# Usage: make create_sql_migration name=projects_table
.PHONY: create_sql_migration
create_sql_migration:
	@timestamp=$$(date +%Y%m%d%H%M%S); \
	filename=app/db/migrations/$${timestamp}_create_$(name).sql; \
	touch $$filename; \
	echo "Created migration file: $$filename"
